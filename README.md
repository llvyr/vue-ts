# vue3-ts-cms

## 接口在线文档

后台 API 接口在线文档【永久有效】：[接口在线文档 访问密码 : llvyr](https://www.apifox.cn/apidoc/shared-7e454f95-9f0d-4f57-adf6-636d9ed39b41)。

## 项目概述

> 一个基于 RBAC 设计的通用后台管理系统，支持通过配置文件批量完成特定功能，如搜索、表格数据显示，导入、导出。项目中封装了大量通用组件，如表单、表格、Echarts 图表、头像上传、面包屑、左侧菜单等。

## 项目技术选型

### 前端技术

- vue3 全家桶
- Typescript
- Element-plus
- axios
- Echarts

### 后端技术

- Egg.js
- Sequelize
- Mysql

### 项目亮点

- RBAC 架构，用户-->角色-->资源的权限配置
- 权限认证
- 动态路由
- ELement-plus 的表格、表单二次封装
- axios 的二次封装
- Echarts 的常用图封装
- 头像裁剪封装
- hooks 的公用逻辑抽取
- 外部 svg 的导入功能
- Excel 的导入和导出功能
- 换肤功能

### 统一规范

- 前端

  - 所的样式、图片放到 asset 下
  - 所有公共组件都放到 base-ui 下
  - 所有与项目相关的组件放到 components 文件下
  - 所欲的全局组件都在 global 文件中注册
  - 所的公用逻辑放到 hook 下
  - 每一个页面的路由进行拆分放到 router 下
  - 所有请求封装放到 servers 下
  - 所有的工具类放到 utils 下
  - 所有页面放到 views 下
  - 所有按需导入外部组件放到 plugins 下注册
  - 所有的请求通过 despatch 调用 vuex 中的 Action 完成，

  ```
  - 一个页面的基本架构
    - xxx 文件夹
      - config 配置信息
        - table.config 表格配置信息
        - search.config 搜索配置信息
        - modal.config 添加、修改模型配置信息
        - export.config 导入导出配置信息配置信息
      - xxx.vue

  ```

  ```
    - 一个组件的基本架构(参考 element 的组件结构)
      - xxx 文件夹
        - src 文件夹
          - xxx.vue
        - types 文件夹
          -index.ts
        - index.ts 根目录
  ```

- 后端
  - controller 公共逻辑处理
    - 参数合法校验成功后调用对应的 service 层
  - exception 统一异常处理
    - 权限异常处理
    - 参数错误处理
    - 404 处理
    - 服务器异常处理
  - extend 扩展
    - 辅助模块 helper
    - 公共方法
  - middleware 插件
    - 权限检测
    - 异常捕获
  - model 数据库映射模型
    - 模型数据定义
    - 模型关联
  - public 公共资源目录
    - 存放上传的头像
  - rules 参数规则校验
    - 存放规则配置信心
  - service 服务层，执行数据库操作
    - 增删改查
    - 返回执行结果
  - router.js 配置路由映射
  - config 项目基础配置 -数据库配置
    - session 配置
    - jwt 配置
    - 插件配置
    - .......

### 项目工关键功能设计

> 登录功能

- 账号登录实现思路
  - 账号和手机号具有唯一性,是辨别用户身份的唯一凭证
  - 新建用户或则批量导入用户数据时后台会进行关键字段检测
  - 用户密码采用 crypto 加密
  - 账号密码正确后会依据 status 字段的值检测账号是否被冻结
  - 用户登录后成功后调用 getUerInfo 接口后台根据 token 自动解析出用户 id 以及用户身份并返回用户角色信息
  - 用户需要通过返回的角色信息调用对应的接口获取用户的权限和菜单等信息
  - 之后对于拥有权限的请求都将进行权限认证.
- 手机号登录实现思路
  - 检测手机号符合规则后可点击发送验证码按钮获取验证码
  - 后台会采用哈希值截取出 6 位作为验证码，同时保存到 session 会话中方便后期做登录验证
  - 其他流程同上

> 换肤功能

- 实现思路
  - 基本流程 通过动态修改 css 自定义变量 var(--primary) 实现页面颜色的改变
  - 定义不同主题的 less 颜色变量 导出变量供页面使用
  - 将默认系统主题存储到 vuex 和本地 storage 中
  - 根据不同的主题动态替换 根目录的 css 变量达到改变样式的效果

> 数字滚动、头像裁剪功能

- 实现思路
  - 主要是采用了 vue-countup-v3 实现数字滚动效果
  - 利用 cropperjs 实现头像裁剪效果
  - `遇到的坑`：数据同步更新问题，出现原因，在 setup 函数会执行一次，及时数据改变也不会触发视图更新，解决方案：使用计算属性(计算属性持续追踪它的响应依赖)

> Echarts 数据分析功能

- 实现思路
  - 采用 Echarts 通过传入配置参数渲染对应的图表库
  - `遇到的坑`：下载地图 json 数据的是否要看清楚里面的省份名称 例如：有的 json 数据是`福建省`有的是`福建`

> 删除、更新、新建功能

- 实现思路

  - 所有的数据都要进行非空判断
  - 对于具有关联的数据删除后台通过先删除所有相关数据,再添加数据的形式完成删除功能
  - 对于具有关联的数据更新需要验证 id 是否存在，以及数据合法性
  - 对具有关联的数据新建需要验证数据合法性
  - 所有涉及到多表关联更新的操作后台都要对数据库进行事务处理
  - 具有关联的数据删除：
    - 角色删除
      - 删除角色 需要判断角色是否和用户关联，存在关联不能删除，不存在关联需要删除当前角色以及角色菜单表中角色所关联的所有菜单菜单
    - 菜单删除：
      - 删除父级菜单，所有的子集菜单都要删除
    - 用户删除
      - 删除用户同时删除与用户关联的一切数据(评论、回复、文章、用户角色关联关系记录、点赞记录、收藏记录)
    - 文章删除
      - 删除一起和文章相关的数据(文章点赞记录、收藏记录、评论记录、评论的子评论记录、文章分类记录)
    - 评论删除
      - 删除一切和此条评论有关的记录(回复记录)

- 页面统一设计
  - 通用模式：views 下的页面组件（传递 props）--> page-xx 组件(传递 props)--> base-ui 先组件(接收 props 并渲染组件)
  - 第一层：base-ui 文件夹下的组件 主要是对 element-plus 的原生组件二次封装 利用 v-bind 绑定 props 接收的属性参数
  - 第二层：components 文件夹先的 page-xx 组件 主要是做请求层业务逻辑，调用 vuex 中的 Action 完成处理所有的请求
  - 第三层：views 下的组件具体的页面如用户、角色页面等
  - 搭建一个新页面需要传递的配置文件

## 项目展示

![账号登录](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/zhanghaodenglu.PNG)
![手机登录](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/shouji.PNG)
![默认主题](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/huanfu.PNG)
![绿色主题](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/grenn.PNG)
![核心技术](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/hexinjishu.PNG)
![头像裁剪](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/touxiang1.PNG)
![数据分析](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/shuju.png)
![角色管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/jiaose.PNG)
![权限管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/quanxianxiugai.PNG)
![权限管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/quanxitianjia.PNG)
![用户管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/yonghu.PNG)
![文章列表](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/wenxhang.PNG)
![文章管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/wenxhang.PNG)
![文章导出](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/wenzhangdaohu.PNG)
![评论管理](http://llvyr.oss-cn-hangzhou.aliyuncs.com/RBAC-AGG/pinglun.PNG)
演示连接[演示连接](https://www.bilibili.com/video/BV1YF41177Ye?share_source=copy_web)

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
