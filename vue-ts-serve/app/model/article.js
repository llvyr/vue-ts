'use strict'

module.exports = (app) => {
	const { STRING, INTEGER, UUID, NOW, DATE, UUIDV4, TEXT } = app.Sequelize

	const Article = app.model.define(
		't_article',
		{
			id: { type: INTEGER, primaryKey: true, autoIncrement: true },
			title: STRING,
			content: TEXT,
			summary: STRING,
			status: STRING,
			view_count: INTEGER,
			cover: STRING,
			level: INTEGER,
			user_id: INTEGER,
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},

		{
			// 去除createAt，updateAt
			timestamps: true,
			// 实例对应的表名
			tableName: 't_article',
		}
	)
	Article.associate = function () {
		Article.belongsToMany(app.model.Category, {
			through: app.model.CategoryArticle,
			foreignKey: 'article_id',
			otherKey: 'cate_id',
			as: 'categories',
		})
		Article.hasMany(app.model.Comment, {
			foreignKey: 'article_id',
			targetKey: 'id',
			as: 'comments',
		})
		Article.hasMany(app.model.ArticleCollect, {
			foreignKey: 'article_id',
			targetKey: 'id',
			as: 'collects',
		})
		Article.hasMany(app.model.ArticleLike, {
			foreignKey: 'article_id',
			targetKey: 'id',
			as: 'likes',
		})
		Article.hasOne(app.model.User, {
			foreignKey: 'id',
			sourceKey: 'user_id',
			as: 'author',
		})
	}
	return Article
}
