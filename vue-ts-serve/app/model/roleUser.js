'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, DATE } = app.Sequelize

	const RoleUser = app.model.define(
		't_role_user',
		{
			id: {
				type: INTEGER,
				primaryKey: true,
				autoIncrement: true,
				allowNull: true,
			},
			role_id: INTEGER,
			user_id: INTEGER,
		},
		{
			tableName: 't_role_user', //指定表名称
			timestamps: false,
		}
	)
	return RoleUser
}
