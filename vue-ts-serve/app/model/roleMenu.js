'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, DATE } = app.Sequelize

	const RoleMenu = app.model.define(
		't_role_menu',
		{
			role_id: INTEGER,
			menu_id: INTEGER,
		},
		{
			tableName: 't_role_menu', //指定表名称
			timestamps: false,
		}
	)
	RoleMenu.associate = function () {
		RoleMenu.belongsTo(app.model.Menu, { foreignKey: 'menu_id' })
	}

	return RoleMenu
}
