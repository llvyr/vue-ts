'use strict'

module.exports = (app) => {
	const { STRING, INTEGER, DATE, BOOLEAN } = app.Sequelize

	const Role = app.model.define(
		't_role',
		{
			id: {
				type: INTEGER,
				autoIncrement: true,
				allowNull: true,
				primaryKey: true,
			},
			name: {
				type: STRING(30),
				primaryKey: true,
			},
			level: INTEGER,
			identification: STRING,
			description: INTEGER,
			status: {
				type: BOOLEAN,
				defaultValue: true,
			},
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},
		{
			tableName: 't_role', //指定表名称
			timestamps: true,
		}
	)
	Role.associate = function () {
		Role.belongsToMany(app.model.User, {
			through: app.model.RoleUser,
			foreignKey: 'role_id',
			otherKey: 'user_id',
			as: 'userRoles',
		})
		Role.belongsToMany(app.model.Menu, {
			through: app.model.RoleMenu,
			foreignKey: 'role_id',
			otherKey: 'menu_id',
			as: 'RoleMenu',
		})
	}

	return Role
}
