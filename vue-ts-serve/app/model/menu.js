'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, BOOLEAN, DATE } = app.Sequelize

	const Menu = app.model.define(
		't_menu',
		{
			id: {
				type: INTEGER,
				primaryKey: true,
				allowNull: true,
			},
			menu_name: STRING,
			parent_pid: INTEGER,
			order_num: INTEGER,
			path: STRING,
			component: STRING,

			is_cache: {
				type: BOOLEAN,
				defaultValue: false,
			},
			menu_type: INTEGER,
			visible: {
				type: BOOLEAN,
				defaultValue: true,
			},
			status: {
				type: INTEGER,
				defaultValue: 1,
			},
			perms: INTEGER,
			icon: STRING,
			remark: STRING,
			createdAt: DATE,
			updatedAt: DATE,
			deletedAt: DATE,
		},
		{
			// 去除createAt，updateAt
			timestamps: true,
			// 实例对应的表名
			tableName: 't_menu',
		}
	)
	Menu.associate = function () {
		Menu.belongsToMany(app.model.Role, {
			through: app.model.RoleMenu,
			foreignKey: 'menu_id',
			otherKey: 'role_id',
			as: 'RoleMenu',
		})
		Menu.associate = function () {
			Menu.belongsTo(app.model.RoleMenu, { foreignKey: 'menu_id' })
		}
	}

	return Menu
}
