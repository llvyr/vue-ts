'use strict'
module.exports = (app) => {
	const { STRING, INTEGER, BOOLEAN, DATE } = app.Sequelize

	const ArticleCollect = app.model.define(
		't_article_collect',
		{
			id: { type: INTEGER, primaryKey: true, autoIncrement: true },
			user_id: INTEGER,
			article_id: INTEGER,
		},
		{
			timestamps: false,
			tableName: 't_article_collect',
		}
	)
	ArticleCollect.associate = function () {
		ArticleCollect.hasOne(app.model.User, {
			foreignKey: 'id',
			sourceKey: 'user_id',
			as: 'collector',
		})
		ArticleCollect.hasOne(app.model.Article, {
			foreignKey: 'id',
			sourceKey: 'article_id',
			as: 'articleDetail',
		})
	}

	return ArticleCollect
}
