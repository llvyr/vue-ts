const BaseService = require('./baseService')
const { Op } = require('sequelize')

class CommentService extends BaseService {
	constructor(...args) {
		super(...args)
		this.modelName = 'Comment'
	}

	/**
	 * 添加一条评论
	 * @param {*} obj
	 * @returns
	 */
	async create(obj) {
		const { form_uid, article_id } = obj
		// 1. 查看 form_ui是否有效
		// 2. 查看 article_ui是否有效
		const IsExistedUid = await this.IsExisted('User', 'id', form_uid)
		if (!IsExistedUid) return { flag: false, message: '用户id不存在' }
		const IsExistedAid = await this.IsExisted('Article', 'id', article_id)
		if (!IsExistedAid) return { flag: false, message: '评论id不存在' }
		// 3 插入数据
		const res = await this.createOne('Comment', obj)
		if (res) {
			return { flag: true, message: '发表评论成功' }
		} else {
			return { flag: false, message: '发表评论失败' }
		}
	}
	/**
	 * 获取评论的回复
	 * @returns
	 */
	async list(options) {
		return await this.ctx.model.Comment.findAndCountAll({
			...options,
			include: [
				{
					model: this.ctx.model.User,
					as: 'userDetail',
					attributes: { exclude: ['password'] },
				},
				{
					model: this.ctx.model.Article,
					as: 'articleDetail',
				},
				{
					model: this.ctx.model.Reply,
					as: 'replies',
					include: [
						{
							model: this.ctx.model.User,
							as: 'userDetail',
							attributes: { exclude: ['password'] },
						},
					],
				},
			],
		})
	}
	/**
	 * 通过id获取 一条评论的所有回复
	 * @param {*} id 要查询的 id
	 * @returns
	 */
	async getInfoById(id) {
		const IsExisted = await this.IsExisted('Comment', 'id', id)
		if (!IsExisted) return { flag: false, message: '评论不存在' }
		let data = await this.ctx.model[this.modelName].findOne({
			where: {
				id: {
					[Op.eq]: id,
				},
			},
			include: [
				{
					model: this.ctx.model.Reply,
					as: 'replies',
				},
			],
		})
		if (data) return { flag: true, message: '查询成功', data }
		else return { flag: true, message: '查询失败' }
	}
	/**
	 * 如果评论被删除，对此条评论的子评论都应该删除
	 * @param {*} ids 删除评论的id
	 */
	async delAll({ ids, transactions }) {
		let transaction = transactions
		try {
			if (!transactions) transaction = await this.ctx.model.transaction()
			for (let i = 0; i < ids.length; i++) {
				const id = ids[i]
				// 评论 id 数据是否存在
				const IsExisted = await this.IsExisted(this.modelName, 'id', id)
				// 只删除一条数据 并且评论不存在
				if (!IsExisted && ids.length === 1)
					return { flag: false, message: '评论不存在' }
				// 删除多条数据 其中出现一条 非法数据 跳过该条数据继续执行删除
				if (!IsExisted && ids.length != 1) continue
				// 执行删除操作
				const res = await this.destroyOne(this.modelName, 'id', id, transaction)
				if (!res) throw Error
				// 删除附表中的数据
				const findReply = await this.ctx.model.Reply.findAll({
					where: {
						comm_id: id,
					},
				})
				if (findReply.length) {
					// 执行评论删除 同时删除评论的子评论
					let ReplayIds = findReply.map((item) => item.dataValues.id)
					const flag = await this.ctx.model.Reply.destroy({
						where: {
							id: ReplayIds,
						},
						transaction,
					})
					if (!flag) throw Error
				}
			}
			if (!transactions) {
				await transaction.commit()
			}
			return {
				flag: true,
				message: '删除成功',
			}
		} catch (error) {
			console.log(error)
			if (!transaction) {
				await transaction.rollback()
			}
			return {
				flag: false,
				message: '删除失败。',
			}
		}
	}
}

module.exports = CommentService
