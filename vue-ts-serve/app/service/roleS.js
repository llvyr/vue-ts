const BaseService = require('./baseService')
const { Op } = require('sequelize')
class RoleService extends BaseService {
	constructor(...args) {
		super(...args)
		this.modelName = 'Role'
	}
}

module.exports = RoleService
