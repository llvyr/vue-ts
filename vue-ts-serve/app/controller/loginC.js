'use strict'
const crypto = require('crypto')
const BaseController = require('./baseController')
class LoginController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'loginS'
	}
	/**
	 *
	 * @returns 账号登录
	 */
	async loginAccount() {
		const { ctx } = this
		const {
			service,
			request: { body },
			helper,
		} = ctx
		const validateResult = await helper.validateResult(
			'user.loginAccount',
			this.ctx.request.body
		)
		// 验证不通过时，阻止后面的代码执行
		if (!validateResult) return
		let { password } = this.ctx.request.body
		const data = await service[this.serviceName].loginAccount(body)

		// 判断返回值
		if (data) {
			// console.log(data.dataValues.status)
			if (!data.dataValues.status) {
				this.fail({ message: '账号已被冻结，请联系管理员' })
				return
			}
			// 账号存在 比对密码
			if (await helper.comparePwd(password, data.password)) {
				//登录成功 生成token 追加到用户信息中
				const userId = data?.id
				delete data.dataValues.password
				data.dataValues.token = await ctx.service.jwt.createToken(userId)
				// 把当前登录用户的id保存到session 中
				this.ctx.session.currentLoginUserId = data.dataValues.id
				this.success({ data })
			} else {
				this.fail({ message: '密码错误' })
			}
		} else {
			this.fail({ message: '账号错误' })
		}
	}

	/**
	 * 手机登录
	 */

	async loginPhone() {
		const { ctx } = this
		const { service, helper } = ctx
		const validateResult = await helper.validateResult(
			'user.loginPhone',
			this.ctx.request.body
		)
		// 验证不通过时，阻止后面的代码执行
		if (!validateResult) return
		let { phone, verifyCode } = ctx.request.body
		const { flag, data, message } = await service.userS.loginPhone(phone)
		// 判断返回值

		if (flag) {
			// 账号存在 对比验证码
			// 获取验证码
			if (!data.dataValues.status) {
				this.fail({ message: '账号已被冻结，请联系管理员' })
				return
			}
			const code = ctx.session.verifyCode
			if (code == verifyCode) {
				//登录成功 生成token 追加到用户信息中
				const userId = data.dataValues.id
				delete data.dataValues.password
				data.dataValues.token = await ctx.service.jwt.createToken(userId)
				this.success({ data })
			} else {
				this.fail({ message: '验证码错误' })
			}
		} else {
			this.fail({ message })
		}
	}

	/**
	 * 随机生成验证码
	 */
	async createdCode() {
		const { ctx } = this
		const code = crypto
			.randomBytes(Math.ceil(6 / 2))
			.toString('hex')
			.slice(0, 6)
		// 保存验证码到session中 方便后期做登录验证
		ctx.session.verifyCode = code
		// 随机延迟相应

		return this.success({ data: { verifyCode: code } })
	}
}

module.exports = LoginController
