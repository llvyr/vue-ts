'use strict'
const Controller = require('egg').Controller
const bcrypt = require('bcryptjs')
class BaseController extends Controller {
	// 请求成功数处理  code 为 1成功 / 0失败
	success(obj) {
		let { data = null, message = 'success', code = 1 } = { ...obj }
		const { ctx } = this
		ctx.status = 200
		ctx.body = {
			code,
			message,
			data,
		}
	}
	fail(obj) {
		let { data = null, message = 'error', code = 0 } = { ...obj }
		const { ctx } = this
		ctx.status = 200
		ctx.body = {
			code,
			message,
			data,
		}
	}
	//GET获取全部 /posts
	async index() {
		const { service, helper } = this.ctx
		let query = this.ctx.query.queryInfo
		let options = helper.whereConcat(query)
		const data = await service[this.serviceName].list(options)
		this.success({ data })
	}
	// GET搜索 /posts/new
	async new() {}
	// POST添加 /posts
	async create() {
		const { service, helper } = this.ctx
		let bodyObj = this.ctx.request.body
		if (this.validateCreatedPath) {
			const validateResult = await helper.validateResult(
				this.validateCreatedPath,
				{
					...bodyObj,
				}
			)

			if (!validateResult) return
		}
		const uniqueWords = this.uniqueWords ? this.uniqueWords : null
		// 更新角色要同时更新角色菜单表
		// 删除角色要查看当前角色是否和用户有关联
		// 不同的操作对应不同的关联模型
		let moduleObj = this.moduleObj ? this.moduleObj : null
		if (this.serviceName === 'roleS') {
			moduleObj = this.createdModuleObj
		}
		const { flag, message } = await service[this.serviceName].create(
			bodyObj,
			uniqueWords,
			moduleObj
		)
		if (flag) {
			this.success({ message })
		} else {
			this.fail({ message })
		}
	}
	// GET根据id 查询 /posts/:id
	async show() {
		const { service, helper } = this.ctx
		const id = this.ctx.params.id
		// 参数校验
		const validateResult = await helper.validateResult('commonValiDate', { id })
		if (!validateResult) return
		const { flag, message, data } = await service[this.serviceName].getInfoById(
			id
		)
		if (flag) {
			this.success({ message, data })
		} else {
			this.fail({ message })
		}
	}
	// POST根据id 获取信息 /posts/:id/edit
	async edit() {}
	// PUT修改 /posts/:id
	async update() {
		const { service, helper } = this.ctx
		let { id } = this.ctx.params
		let bodyObj = this.ctx.request.body
		if (this.validateUpdatePath) {
			const validateResult = await helper.validateResult(
				this.validateUpdatePath,
				{
					...bodyObj,
					id,
				}
			)
			if (!validateResult) return
		}
		const uniqueWords = this.uniqueWords ? this.uniqueWords : null
		let moduleObj = null
		let arr = ['roleS'] // 需要特别处理的模型
		if (arr.includes(this.serviceName)) {
			moduleObj = this.createdModuleObj
		} else {
			moduleObj = this.moduleObj
		}
		const { flag, message } = await service[this.serviceName].update(
			id,
			bodyObj,
			uniqueWords,
			moduleObj
		)
		if (flag) {
			this.success({ message })
		} else {
			this.fail({ message })
		}
	}
	// DELETE 删除 /posts/:id
	async destroy() {}
	// 批量删除
	async delAll() {
		const { service, helper } = this.ctx
		let { ids } = this.ctx.request.body
		if (!Array.isArray(ids)) ids = [ids]
		if (this.serviceName == 'roleS' && ids.includes(1)) {
			this.fail({ message: '超级管理员不能删除' })
			return
		}
		const validateResult = await helper.validateResult('commonValiDate', {
			id: ids,
		})
		if (!validateResult) return
		const moduleObj = this.moduleObj ? this.moduleObj : null
		let model2 = null
		let arr = ['roleS'] // 需要特别处理的模型
		if (arr.includes(this.serviceName)) {
			model2 = this.createdModuleObj
		}
		const { flag, message } = await service[this.serviceName].delAll({
			ids,
			moduleObj,
			model2,
		})
		if (flag) {
			this.success({ message })
		} else {
			this.fail({ message })
		}
	}
	// post 模糊查询
	async findLike() {
		const { service, helper } = this.ctx
		let body = this.ctx.request.body.queryInfo
		// 处理关联关系的数据查询
		let associateModalWhere = body?.otherWhere?.associateModal
		if (associateModalWhere) {
			delete body.otherWhere.associateModal
		}
		let options = helper.whereConcat(body)
		let moduleObj = this.moduleObj ? this.moduleObj : null
		let arr = ['roleS'] // 需要特别处理的模型
		if (arr.includes(this.serviceName)) {
			moduleObj = this.createdModuleObj
		}
		let orderKey = this.moduleObj?.orderKey // 需要对关联模型排序的字段
		// console.log(options)
		const data = await service[this.serviceName].list(
			options,
			moduleObj,
			associateModalWhere,
			orderKey
		)
		this.success({ data })
	}
	// 批量插入
	async bulkCreates() {
		let body = this.ctx.request.body
		const { service, helper } = this.ctx
		const uniqueWords = this.uniqueWords ? this.uniqueWords : null
		let moduleObj = this.moduleObj ? this.moduleObj : null
		// 对用户的单独处理password字段
		if (this.serviceName == 'userS') {
			const salt = this.app.config.jwt.secret.secret
			const pwd = bcrypt.hashSync('123456', salt)
			body.forEach((item) => {
				if (!item.password) {
					item.password = pwd
				}
			})
		}
		if (this.serviceName == 'roleS') {
			// 批量插入角色 不需要创建用户
			moduleObj = null
		}

		const { flag, data, message } = await service[this.serviceName].bulkCreates(
			body,
			uniqueWords,
			moduleObj
		)

		if (flag) {
			this.success({ data, message })
		} else {
			this.fail({ data, message })
		}
	}
}

module.exports = BaseController
