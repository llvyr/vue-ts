'use strict'

const BaseController = require('./baseController')
class UserController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'userS' // serviceName
		this.uniqueWords = ['username', 'phone'] // 表中唯一的字段 列名称
		this.validateUpdatePath = 'user.updatedUser' // 对应的修改的参数校验规则
		this.validateCreatedPath = 'user.createdUser' //对应的创建的参数校验规则

		this.moduleObj = {
			orderKey: 'level', // 排序字段
			isDelAssociate: true, // 如果数据存在关联 删除时是否能一起删除
			associateModelName: 'RoleUser', // 多表关联的中间关联模型的名称
			associateModelForeignKey: 'user_id', // 模型的主键
			associateModelOtherKey: 'role_id', // 模型的关联外键
			associateModelAs: 'userRoles', // 关联模型的别名
			associateModelOtherModel: 'Role', // 附表的模型名称
			associateModelConfig: [true, true, true, true], // 关联模型的配置   增 删 改 查  true/是  false/否
			//含义 ： 添加用户RoleUser表需要一起更新  每一个新建用户都有【普通用户】的角色
			//含义 ： 修改用户RoleUser表需要一起更新  如果是修改角色信息 RoleUser表需要一起更新
			//含义 ： 删除用户RoleUser表需要一起更新  删除 RoleUser 表中 所有uer_id 为 id 的数据
			//含义 ： 查询用户RoleUser表需使用到      查询用户需要将 对应的角色信息查出来
		}
	}

	// 根据id获取用户信息
	async getUserInfoById() {
		const { service, helper } = this.ctx
		const id = this.ctx.params.id
		// 参数校验
		const validateResult = await helper.validateResult('commonValiDate', {
			id,
		})
		if (!validateResult) return
		// 解析token 拿到id

		const { flag, message, data } = await service[this.serviceName].getInfoById(
			id
		)
		if (flag) {
			this.success({ data })
		} else {
			this.fail({ message })
		}
	}
	/**
	 * 获取用户信息
	 */
	async getUserInfo() {
		// 获取请求头信息
		let token = this.ctx.request.header.authorization

		const res = await this.service.jwt.verifyToken(token)

		if (res) {
			// 解析token 获取 id
			const id = await this.service.jwt.getUserIdFromToken(token)
			// 通过id 获取用户信息
			let { flag, data, message } = await this.service[
				this.serviceName
			].getInfoById(id)
			data = data.rows[0]
			if (flag) {
				delete data?.dataValues.password
				this.success({ data })
			} else {
				this.fail({ message })
			}
		}
	}

	/**
	 * 重置密码
	 */
	async resetPassword() {
		const { ctx } = this
		let {
			request: { body },
			service,
		} = ctx
		if (!body.id) return this.fail({ message: 'id关键字缺失！' })
		const res = await service[this.serviceName].resetPassword(body)
		if (res) {
			this.success({ message: '密码修改成功！' })
		} else {
			this.fail({ message: '密码修改改失败！' })
		}
	}

	/**
	 * 用户点赞
	 *
	 */
	async thumbsUp() {
		const { service, params } = this.ctx
		const { u_id, art_id } = params
		if (!u_id) return this.fail({ message: '用户id不能为空' })
		if (!art_id) return this.fail({ message: '点赞文章id不能为空' })
		const { flag, message } = await service.userS.thumbsUp(u_id, art_id)
		if (flag) {
			this.success({ message })
		} else {
			this.fail({ message })
		}
	}
	/**
	 *用户给收藏
	 *
	 */
	async collect() {
		const { service, params } = this.ctx
		const { u_id, art_id } = params
		if (!u_id) return this.fail({ message: '用户id不能为空' })
		if (!art_id) return this.fail({ message: '收藏文章id不能为空' })
		const { flag, message } = await service.userS.collect(u_id, art_id)
		if (flag) {
			this.success({ message })
		} else {
			this.fail({ message })
		}
	}
}

module.exports = UserController
