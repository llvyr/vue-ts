const Controller = require('./baseSingleUploadFile')

class SingleUploadFileControl extends Controller {
	async createAvatar() {
		const { service } = this.ctx
		// 上传头像的,会在uploads文件夹下有个avatar的文件夹下面才是2019、06、21
		const { url, fields } = await this.singleUploadFile('avatar')
		const { id } = this.ctx.params

		let { flag, message } = await service.userS.update(id, {
			avatar: `${this.app.config.baseUrl}${url}`,
		})
		if (flag) {
			this.success({
				data: `${this.app.config.baseUrl}${url}`,
				message: '上传文件成功',
			})
		} else {
			this.fail({ message })
		}
	}
	async createArticleCover() {
		const { service } = this.ctx
		const { url, fields } = await this.singleUploadFile('article')
		const { id } = this.ctx.params
		let { flag, message } = await service.articleS.update(id, {
			cover: `${this.app.config.baseUrl}${url}`,
		})
		if (flag) {
			this.success({
				data: `${this.app.config.baseUrl}${url}`,
				message: '上传文件成功',
			})
		} else {
			this.fail({ message })
		}
	}
}
module.exports = SingleUploadFileControl
