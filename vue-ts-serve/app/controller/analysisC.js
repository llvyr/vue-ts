'use strict'
const BaseController = require('./baseController')
class AnalysisController extends BaseController {
	/**
	 * 获取用户、角色、菜单的总数
	 */
	async total() {
		const { service } = this.ctx
		const { flag, data } = await service.analysisS.total()
		if (flag) {
			this.success({ data })
		}
	}
	/**
	 * 根据 角色进 行分组
	 */
	async userByRole() {
		const { service } = this.ctx
		const data = await service.analysisS.userByRole()
		if (data) {
			this.success({ data })
		}
	}
	async userGroupByGender() {
		const { service } = this.ctx
		const data = await service.analysisS.userGroupByGender()
		if (data) {
			this.success({ data })
		}
	}
}

module.exports = AnalysisController
