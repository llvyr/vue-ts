'use strict'
const BaseController = require('./baseController')

class CommentController extends BaseController {
	constructor(...args) {
		super(...args)
		this.serviceName = 'commentS'
		this.validateUpdatePath = 'comment.updatedComment' // 对应的修改的参数校验规则
		this.validateCreatedPath = 'comment.createdComment' //对应的创建的参数校验规则
	}
}
module.exports = CommentController
