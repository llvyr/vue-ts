'use strict'

const rule = {
	title: [
		{ required: true, message: '文章名称不能为空' },
		{
			min: 2,
			max: 20,
			message: '文章标题2-20位有效字符',
		},
	],
	content: [
		{ required: true, message: '文章内容不能为空' },
		{
			min: 2,
			message: '文章内容至少2位有效字符',
		},
	],
}

module.exports = rule
