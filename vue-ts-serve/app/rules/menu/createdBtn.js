'use strict'

const rule = {
	menu_name: [
		{ required: true, message: '菜单名称不能为空' },
		{
			min: 2,
			message: '菜单格式为至少2位有效字符',
		},
	],
	perms: [
		{ required: true, message: 'perms权限标识不能为空' },
		{
			min: 2,
			max: 100,
			message: 'perms权限标识格式2-100位有效字符',
		},
	],
}

module.exports = rule
