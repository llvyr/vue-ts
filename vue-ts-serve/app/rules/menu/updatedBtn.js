'use strict'

const rule = {
	id: [{ required: true, message: 'id不能为空' }],
	menu_name: [
		{ message: '角色名称不能为空' },
		{
			min: 2,
			message: '角色格式为至少2位有效字符',
		},
	],
	perms: [
		{ message: 'perms权限标识不能为空' },
		{
			min: 2,
			max: 100,
			message: 'perms权限标识格式2-100位有效字符',
		},
	],
}

module.exports = rule
