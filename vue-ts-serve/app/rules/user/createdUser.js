'use strict'

const rule = {
	username: [
		{ required: true, message: '账号不能为空' },
		{
			min: 2,
			max: 10,
			message: '账号格式为3-10位有效字符',
		},
	],
	password: [
		{ required: true, message: '密码不能为空' },
		{
			pattern: /^[a-zA-Z0-9]{4,9}$/,
			message: '密码格式为4-9位数字或字母',
		},
	],
	gender: [
		{
			type: 'enum',
			enum: [0, 1, 2],
			message: '性别为:0,1,2的其中一个,0保密,1男，2女',
		},
	],
}

module.exports = rule
