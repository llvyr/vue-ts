'use strict'

const rule = {
	username: [
		{ required: true, message: '账号不能为空' },
		{
			min: 2,
			max: 10,
			message: '账号格式为3-10位有效字符',
		},
	],
	password: [
		{ required: true, message: '密码不能为空' },
		{
			pattern: /^[a-zA-Z0-9]{4,9}$/,
			message: '密码格式为4-9位数字或字母',
		},
		// {
		// 	validator(rule, value, callback, source, options) {
		// 		const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[^]{8,16}$/
		// 		if (pattern.test(value)) {
		// 			callback() // 验证通过
		// 			return
		// 		}
		// 		callback({ message: '密码最少包含一个大小写字母、数字并且为8-16位' }) // 验证不通过
		// 	},
		// },
	],
}

module.exports = rule
