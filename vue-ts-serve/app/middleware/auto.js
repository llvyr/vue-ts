// app/middleware/auth.js
const AuthException = require('../exception/auth')
const { Op } = require('sequelize')
module.exports = (name) => {
	return async function auth(ctx, next) {
		// 获取token
		const authorization = ctx.request.header.authorization
		if (!authorization) throw new AuthException('未携带token', 401)
		const token = authorization?.replace('Bearer ', '')
		// 通过token获取用户id
		const userId = await ctx.service.jwt.getUserIdFromToken(token)
		// 校验权限
		await checkAuth(userId, ctx)
		await next()
	}
	async function checkAuth(userId, ctx) {
		if (!name) {
			return true
		}
		// 查询菜单是否存在
		const menu = await ctx.model.Menu.findOne({
			where: { perms: name },
		})
		if (menu === null) {
			return true
		}
		// 查询用户绑定的角色
		const roles = await ctx.model.RoleUser.findAll({
			attributes: ['role_id'],
			where: { user_id: userId },
		})
		const roleIds = roles.map((item) => item.role_id)
		if (roleIds.includes(1)) {
			return true
		}
		// 查询用户是否有菜单的权限
		const hasAccess = await ctx.model.RoleMenu.findOne({
			where: { role_id: { [Op.in]: roleIds }, menu_id: menu.id },
		})
		if (hasAccess === null) {
			throw new AuthException('权限不足', 403)
		}
	}
}
