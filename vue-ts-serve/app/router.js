'use strict'

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = (app) => {
	const { router, controller } = app
	const BAST_URL = '/system'
	// 实例化auth中间件
	const auth = app.middleware.auto

	//////////////////////////////////login start//////////////////////////////////
	router.post(`${BAST_URL}/loginAccount`, controller.loginC.loginAccount)
	router.get(`${BAST_URL}/createdCode`, controller.loginC.createdCode)
	router.post(`${BAST_URL}/loginPhone`, controller.loginC.loginPhone)

	//////////////////////////////////login end//////////////////////////////////

	//////////////////////////////////users start//////////////////////////////////
	router.get(`${BAST_URL}/users/userById/:id`, controller.userC.getUserInfoById)
	router.get(`${BAST_URL}/users/userInfo`, controller.userC.getUserInfo)
	router.post(
		`${BAST_URL}/users/userList`,
		auth('system:user:query'),
		controller.userC.findLike
	)
	router.delete(
		`${BAST_URL}/users/delAll`,
		auth('system:user:delete'),
		controller.userC.delAll
	)
	router.post(
		`${BAST_URL}/users/bulkCreate`,
		auth('system:user:create'),
		controller.userC.bulkCreates
	)
	router.post(
		`${BAST_URL}/users/resetPassword`,
		auth('system:user:update'),
		controller.userC.resetPassword
	)
	router.post(
		`${BAST_URL}/users`,
		auth('system:user:create'),
		controller.userC.create
	)
	router.get(
		`${BAST_URL}/users`,
		auth('system:user:query'),
		controller.userC.index
	)
	router.put(
		`${BAST_URL}/users/:id`,
		auth('system:user:update'),
		controller.userC.update
	)
	router.get(
		`${BAST_URL}/users/:id`,
		auth('system:user:query'),
		controller.userC.show
	)
	router.post(
		`${BAST_URL}/users/thumbsUp/:u_id/:art_id`,
		controller.userC.thumbsUp
	)
	router.post(
		`${BAST_URL}/users/collect/:u_id/:art_id`,
		controller.userC.collect
	)

	// router.resources('users', `${BAST_URL}/users`, controller.userC)
	//////////////////////////////////users end//////////////////////////////////

	//////////////////////////////////role start//////////////////////////////////
	router.delete(
		`${BAST_URL}/roles/delAll`,
		auth('system:role:delete'),
		controller.roleC.delAll
	)
	router.post(`${BAST_URL}/roles/roleList`, controller.roleC.findLike)
	router.post(
		`${BAST_URL}/roles/bulkCreate`,
		auth('system:role:created'),
		controller.roleC.bulkCreates
	)

	router.post(
		`${BAST_URL}/roles`,
		auth('system:role:create'),
		controller.roleC.create
	)
	router.get(
		`${BAST_URL}/roles`,
		auth('system:role:query'),
		controller.roleC.index
	)
	router.put(
		`${BAST_URL}/roles/:id`,
		auth('system:role:update'),
		controller.roleC.update
	)
	router.get(
		`${BAST_URL}/roles/:id`,
		auth('system:role:query'),
		controller.roleC.show
	)

	// router.resources('roles', `${BAST_URL}/roles`, controller.roleC)
	//////////////////////////////////role end//////////////////////////////////

	//////////////////////////////////menu start//////////////////////////////////
	router.delete(
		`${BAST_URL}/menus/delAll`,
		auth('system:menu:delete'),
		controller.menuC.delAll
	)
	router.post(`${BAST_URL}/menus/treeMenus`, controller.menuC.treeMenu)
	router.post(`${BAST_URL}/menus/menuList`, controller.menuC.findLike)
	router.post(
		`${BAST_URL}/menus/treeMenusByRoleIds`,
		controller.menuC.treeMenuByRoleIds
	)

	router.post(
		`${BAST_URL}/menus`,
		auth('system:menu:create'),
		controller.menuC.create
	)
	router.get(
		`${BAST_URL}/menus`,
		auth('system:menu:query'),
		controller.menuC.index
	)
	router.put(
		`${BAST_URL}/menus/:id`,
		auth('system:menu:update'),
		controller.menuC.update
	)
	router.get(
		`${BAST_URL}/menus/:id`,
		auth('system:menu:query'),
		controller.menuC.show
	)

	// router.resources('menus', `${BAST_URL}/menus`, controller.menuC)
	//////////////////////////////////menu end//////////////////////////////////

	//////////////////////////////////analysis start//////////////////////////////////

	router.get(`${BAST_URL}/analysis/total`, controller.analysisC.total)
	router.get(`${BAST_URL}/analysis/userByRole`, controller.analysisC.userByRole)
	router.get(
		`${BAST_URL}/analysis/userGroupByGender`,
		controller.analysisC.userGroupByGender
	)

	//////////////////////////////////analysis end//////////////////////////////////

	//////////////////////////////////file start//////////////////////////////////
	router.post(
		`${BAST_URL}/file/uploadAvatar/:id`,
		auth('system:user:update'),
		controller.singleUploadFileC.createAvatar
	)
	router.post(
		`${BAST_URL}/file/createArticleCover/:id`,
		auth('system:article:update'),
		controller.singleUploadFileC.createArticleCover
	)
	router.post(
		`${BAST_URL}/uploadFiles`,
		controller.multiUploadFilesC.createAvatar
	)

	//////////////////////////////////file end//////////////////////////////////

	//////////////////////////////////article end//////////////////////////////////

	router.post(
		`${BAST_URL}/articles/articleList`,
		auth('system:article:query'),
		controller.articleC.findLike
	)
	router.delete(
		`${BAST_URL}/articles/delAll`,
		auth('system:article:delete'),
		controller.articleC.delAll
	)
	router.put(
		`${BAST_URL}/articles/:id`,
		auth('system:article:update'),
		controller.articleC.update
	)
	router.get(
		`${BAST_URL}/articles/:id`,
		auth('system:article:query'),
		controller.articleC.show
	)
	router.post(
		`${BAST_URL}/articles`,
		auth('system:article:create'),
		controller.articleC.create
	)

	//////////////////////////////////article end//////////////////////////////////

	//////////////////////////////////category end//////////////////////////////////

	router.post(
		`${BAST_URL}/categorys/categoryList`,
		auth('system:category:query'),
		controller.categoryC.findLike
	)
	router.delete(
		`${BAST_URL}/categorys/delAll`,
		auth('system:category:delete'),
		controller.categoryC.delAll
	)
	router.put(
		`${BAST_URL}/categorys/:id`,
		auth('system:category:update'),
		controller.categoryC.update
	)
	router.get(
		`${BAST_URL}/categorys/:id`,
		auth('system:category:query'),
		controller.categoryC.show
	)
	router.post(
		`${BAST_URL}/categorys`,
		auth('system:category:create'),
		controller.categoryC.create
	)
	//////////////////////////////////category end//////////////////////////////////

	//////////////////////////////////comment start//////////////////////////////////

	router.post(
		`${BAST_URL}/comments/commentList`,
		auth('system:comment:query'),
		controller.commentC.findLike
	)
	router.delete(
		`${BAST_URL}/comments/delAll`,
		auth('system:comment:delete'),
		controller.commentC.delAll
	)
	router.put(
		`${BAST_URL}/comments/:id`,
		auth('system:comment:update'),
		controller.commentC.update
	)
	router.get(
		`${BAST_URL}/comments/:id`,
		auth('system:comment:query'),
		controller.commentC.show
	)
	router.post(
		`${BAST_URL}/comments`,
		auth('system:comment:create'),
		controller.commentC.create
	)

	//////////////////////////////////comment end//////////////////////////////////
}
