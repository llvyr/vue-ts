'use strict'
const path = require('path')
const os = require('os')
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
	/**
	 * built-in config
	 * @type {Egg.EggAppConfig}
	 **/
	const config = (exports = {})

	// use for cookie sign key, should change to your own and keep security
	config.keys = appInfo.name + '_1649581243918_9688'

	// add your middleware config here
	config.middleware = []

	// add your user config here
	const userConfig = {
		// myAppName: 'egg',
	}
	// 配置需要的中间件，数组顺序即为中间件的加载顺序
	config.middleware = ['errorHandler']

	// 跨域配置
	config.security = {
		csrf: {
			enable: false,
		},
		domainWhiteList: ['*'], // 允许访问接口的白名单，例如：http://localhost:8080 *表示均可访问
	}
	// 跨域配置
	config.cors = {
		origin: '*',
		allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS',
	}
	// jwt 过期时间为7天
	config.jwt = {
		expiresIn: 3600 * 24 * 7,
		secret: 'b2ce49e4a541068d',
	}
	// session
	config.session = {
		key: 'SESSION_ID',
		maxAge: 24 * 3600 * 1000,
		renew: true, //延长会话有效期
	}
	// 数据库配置
	config.sequelize = {
		dialect: 'mysql',
		host: 'localhost',
		port: 3306,
		database: 'vue3ts',
		username: 'root',
		password: '123456',
		// 配置数据库时间为东八区北京时间
		timezone: '+08:00',
		define: {
			// model的全局配置
			timestamps: true, // 添加create,update,delete时间戳
			paranoid: true, // 添加软删除 不能进行回滚
			freezeTableName: true, // 表名不为复数
			underscored: false, // 驼峰式字段被默认转为下划线
		},
		// 打印日志
		logging: true,
		// 时间格式化
		dialectOptions: {
			dateStrings: true,
			typeCast: true,
		},
	}
	// 规则验证
	config.validatePlus = {
		convert: true,
		widelyUndefined: true,
		resolveError(ctx, errors) {
			console.log(errors)
			if (errors.length) {
				ctx.type = 'json'
				ctx.status = 200
				ctx.body = {
					code: 400,
					data: null,
					message: errors[0]?.message || errors || '参数格式错误',
				}
			}
		},
	}
	// 文件上传
	config.multipart = {
		mode: 'stream',
		whitelist: ['.jpg', '.jpeg', '.png', '.gif', '.webp'],
		fileSize: '50mb',
		fieldSize: '1024kb',
		fileExtensions: [''], // 扩展几种上传的文件格式
		tmpdir: path.join(os.tmpdir(), 'egg-multipart-tmp', appInfo.name), // 配置文件缓存目录
		cleanSchedule: {
			cron: '0 30 4 * * *', // 自动清除时间
		},
	}
	// 上传文件路径路径 做好是放到public文件夹，egg默认通过浏览器可以访问到public下的资源
	config.uploadDir = 'app/public/uploads'

	config.baseUrl = 'http://127.0.0.1:7001'
	return {
		...config,
		...userConfig,
	}
}
