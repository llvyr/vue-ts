'use strict'

/** @type Egg.EggPlugin */
// module.exports = {
// 	// had enabled by egg
// 	// static: {
// 	//   enable: true,
// 	// }
// }
exports.cors = {
	enable: true,
	package: 'egg-cors',
}

exports.sequelize = {
	enable: true,
	package: 'egg-sequelize',
}
exports.jwt = {
	enable: true,
	package: 'egg-jwt',
}
exports.validatePlus = {
	enable: true,
	package: 'egg-validate-plus',
}
exports.session = {
	key: 'EGG_SESS',
	maxAge: 24 * 3600 * 1000, // 1 day httpOnly: true,
	encrypt: true,
}
exports.oss = {
	enable: true,
	package: 'egg-oss',
}
