/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : vue3ts

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 13/05/2022 18:22:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章名称',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文章内容',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章简介',
  `cover` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面',
  `level` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '等级',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '作者',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态 0 禁止  1 启用  ',
  `view_count` int(11) NULL DEFAULT NULL COMMENT '浏览数量 暂时没用',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` VALUES (1, '不忘初心，勉励前行。', '小白自学前端进阶中...', '一个默默无闻的小前端', 'https://www.orcode.com/uploads/avatar/userimg/71.jpg', '1', 57, '1', 11, '2022-05-09 21:43:10', '2022-05-11 21:40:49', NULL);
INSERT INTO `t_article` VALUES (2, 'vue3+Typescript', '你好vue3', '一个基于RBAC设计的Vue3+TS的论坛系统', 'http://127.0.0.1:7001/public\\uploads\\article\\2022\\05\\08\\1652011218196508.jpg', '1', 57, '1', 11, '2022-05-09 21:43:12', '2022-05-11 21:40:24', NULL);
INSERT INTO `t_article` VALUES (3, '测试', '终于完工了', '测试', 'https://www.orcode.com/uploads/avatar/userimg/71.jpg', '1', 57, '1', 11, '2022-05-09 21:43:14', '2022-05-11 23:36:03', NULL);

-- ----------------------------
-- Table structure for t_article_collect
-- ----------------------------
DROP TABLE IF EXISTS `t_article_collect`;
CREATE TABLE `t_article_collect`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL COMMENT '文章id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article_collect
-- ----------------------------

-- ----------------------------
-- Table structure for t_article_like
-- ----------------------------
DROP TABLE IF EXISTS `t_article_like`;
CREATE TABLE `t_article_like`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL COMMENT '被点赞文章id',
  `user_id` int(11) NOT NULL COMMENT '点赞用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article_like
-- ----------------------------

-- ----------------------------
-- Table structure for t_cate_article
-- ----------------------------
DROP TABLE IF EXISTS `t_cate_article`;
CREATE TABLE `t_cate_article`  (
  `cate_id` int(11) NOT NULL COMMENT '分类id',
  `article_id` int(11) NOT NULL COMMENT '文章id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_cate_article
-- ----------------------------
INSERT INTO `t_cate_article` VALUES (2, 1);
INSERT INTO `t_cate_article` VALUES (3, 3);

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `status` int(1) NULL DEFAULT 1 COMMENT '禁用分类 1 正常 0禁用',
  `identification` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类标识',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类描述',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES (1, '技术类', 1, 'Technology', '技术类文章', '2022-02-20 18:48:38', '2022-05-11 21:33:37', NULL);
INSERT INTO `t_category` VALUES (2, '生活', 1, 'Life', '记录生活', '2022-02-20 19:01:27', '2022-05-11 21:33:46', NULL);
INSERT INTO `t_category` VALUES (3, '项目', 1, 'Project', '项目概述', '2022-02-20 19:01:38', '2022-05-11 21:34:39', NULL);
INSERT INTO `t_category` VALUES (4, '其他', 1, 'Other', '其他', '2022-02-20 19:01:38', '2022-05-11 21:34:52', NULL);

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_uid` int(11) NOT NULL COMMENT '评论用户id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论内容',
  `article_id` int(255) NOT NULL COMMENT '被评论文章的id',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES (1, 27, '测试1', 2, '2022-05-09 13:23:12', NULL, NULL);
INSERT INTO `t_comment` VALUES (2, 27, '评论测试', 2, '2022-05-09 13:30:40', '2022-05-11 21:43:17', NULL);
INSERT INTO `t_comment` VALUES (3, 57, '评论测试', 2, '2022-05-09 13:32:37', '2022-05-11 21:43:14', NULL);

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_pid` int(11) NOT NULL COMMENT '父菜单id',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由跳转途径（全路径）',
  `component` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `is_cache` tinyint(1) NULL DEFAULT 1 COMMENT '是否保持激活 （路由缓存）',
  `menu_type` int(11) NOT NULL COMMENT '分类 0 菜单标题  1 路由（二级菜单）2 权限按钮 \r\n',
  `visible` tinyint(1) NULL DEFAULT 1 COMMENT '菜单显示(0显示，1隐藏)',
  `status` int(255) NULL DEFAULT NULL COMMENT '菜单状态（1正常，0停用）',
  `perms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限编码',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标（菜单才填）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createdAt` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updatedAt` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deletedAt` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES (2, '系统管理', 0, NULL, '', '', 1, 0, 1, 1, '', 'setting', 'RBAC核心管理', '2022-03-10 20:37:57', '2022-05-13 16:51:35', NULL);
INSERT INTO `t_menu` VALUES (3, '文章管理', 0, NULL, '', '', 1, 0, 1, 1, '', 'article', '文章的管理功能，包括文章和分类管理', '2022-03-10 20:37:57', '2022-05-10 19:37:22', NULL);
INSERT INTO `t_menu` VALUES (5, '角色管理', 2, NULL, '/main/system/role', 'role', 1, 1, 1, 1, '', NULL, '系统角色的增删改查等功能', '2022-03-10 20:37:57', '2022-05-11 16:39:39', NULL);
INSERT INTO `t_menu` VALUES (6, '角色管理添加', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:create', NULL, NULL, '2022-03-10 20:37:57', '2022-05-06 17:19:14', NULL);
INSERT INTO `t_menu` VALUES (21, '角色管理修改', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:update', NULL, NULL, '2022-03-10 20:37:57', '2022-03-11 16:20:45', NULL);
INSERT INTO `t_menu` VALUES (22, '角色管理删除', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:delete', NULL, NULL, '2022-03-10 20:37:57', '2022-03-11 16:21:28', NULL);
INSERT INTO `t_menu` VALUES (23, '角色管理查询', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:query', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 09:58:11', NULL);
INSERT INTO `t_menu` VALUES (24, '角色管理批量导入', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:import', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 09:57:19', NULL);
INSERT INTO `t_menu` VALUES (25, '角色管理批量导出', 5, NULL, NULL, NULL, 1, 2, 1, 1, 'system:role:export', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 09:37:34', NULL);
INSERT INTO `t_menu` VALUES (26, '角色管理打印', 5, NULL, NULL, NULL, 1, 2, 0, 1, 'system:role:print', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 09:33:26', '2022-05-10 16:09:17');
INSERT INTO `t_menu` VALUES (27, '权限管理', 2, NULL, '/main/system/menu', 'menu', 1, 1, 1, 1, '', NULL, '权限的增删改查等功能', '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (28, '权限管理添加', 27, NULL, NULL, NULL, 1, 2, 1, 1, 'system:menu:create', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (29, '权限管理修改', 27, NULL, NULL, NULL, 1, 2, 1, 1, 'system:menu:update', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (30, '权限管理删除', 27, NULL, NULL, NULL, 1, 2, 1, 1, 'system:menu:delete', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (31, '权限管理查询', 27, NULL, NULL, NULL, 1, 2, 1, 1, 'system:menu:query', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (32, '用户管理', 2, NULL, '/main/system/user', 'user', 1, 1, 1, 1, '', NULL, '用户的增删改查等功能', '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (33, '用户管理添加', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:create', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (34, '用户管理修改', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:update', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (35, '用户管理删除', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:delete', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (36, '用户管理查询', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:query', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (37, '用户管理批量导入', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:import', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (38, '用户管理批量导出', 32, NULL, NULL, NULL, 1, 2, 1, 1, 'system:user:export', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (39, '用户管理打印', 32, NULL, NULL, NULL, 1, 2, 0, 1, 'system:user:print', NULL, NULL, '2022-03-10 20:37:57', NULL, '2022-05-10 16:09:34');
INSERT INTO `t_menu` VALUES (41, '文章列表', 3, NULL, '/main/system/article', 'article', 1, 1, 1, 1, '', NULL, '文章的增删改查等功能', '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (43, '用户详情', 32, NULL, '/home/userdetail/:id', 'user-detail', 1, 1, 0, 1, '', NULL, NULL, '2022-03-10 20:37:57', NULL, '2022-05-10 16:09:45');
INSERT INTO `t_menu` VALUES (44, '文章列表添加', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:create', NULL, NULL, '2022-03-10 20:37:57', '2022-05-06 17:11:23', NULL);
INSERT INTO `t_menu` VALUES (45, '文章列表修改', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:update', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (46, '文章列表删除', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:delete', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (47, '文章列表查询', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:query', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (48, '文章列表导入', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:import', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (49, '文章列表导出', 41, NULL, NULL, NULL, 1, 2, 1, 1, 'system:article:export', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (50, '文章分类', 3, NULL, '/main/system/category', 'categorical', 1, 1, 1, 1, '', NULL, '文章的分类管理', '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (52, '文章分类添加', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:create', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (53, '文章分类修改', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:update', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (54, '文章分类删除', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:delete', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (55, '文章分类查询', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:query', NULL, NULL, '2022-03-10 20:37:57', NULL, NULL);
INSERT INTO `t_menu` VALUES (56, '文章分类导入', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:import', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 10:25:46', NULL);
INSERT INTO `t_menu` VALUES (57, '文章分类导出', 50, NULL, NULL, NULL, 1, 2, 1, 1, 'system:category:export', NULL, NULL, '2022-03-10 20:37:57', '2022-03-13 10:24:12', NULL);
INSERT INTO `t_menu` VALUES (58, '创建文章', 3, NULL, '/main/system/create_crticle', 'create-article', 1, 1, 0, 1, '', NULL, 'Markdown创建文章', '2022-03-12 14:31:43', '2022-05-10 12:46:49', '2022-05-10 14:41:08');
INSERT INTO `t_menu` VALUES (59, '文章详情', 3, NULL, '/main/system/artical_detail/:id', 'artical-detail', 1, 1, 0, 1, '', NULL, '显示文章详情', '2022-03-12 19:08:27', '2022-05-10 12:55:28', '2022-05-10 14:41:11');
INSERT INTO `t_menu` VALUES (70, '重置密码', 32, NULL, NULL, NULL, 0, 2, 1, 1, 'system:user:restPwd', NULL, '', '2022-05-06 21:48:49', '2022-05-06 21:48:49', NULL);
INSERT INTO `t_menu` VALUES (72, '评论管理', 0, NULL, NULL, NULL, 0, 0, 1, 1, NULL, 'comment', '评论模块', '2022-05-10 16:15:27', '2022-05-10 19:30:15', NULL);
INSERT INTO `t_menu` VALUES (73, '评论列表', 72, NULL, '/main/system/comment', 'comment', 1, 1, 1, 1, NULL, NULL, '', '2022-05-10 16:17:13', '2022-05-10 16:17:13', NULL);
INSERT INTO `t_menu` VALUES (74, '评论列表查询', 73, NULL, NULL, NULL, 0, 2, 1, 1, 'system:comment:query', NULL, '', '2022-05-10 16:46:58', '2022-05-10 16:48:53', NULL);
INSERT INTO `t_menu` VALUES (75, '评论列表删除', 73, NULL, NULL, NULL, 0, 2, 1, 1, 'system:comment:delete', NULL, '', '2022-05-10 16:48:36', '2022-05-10 16:48:36', NULL);
INSERT INTO `t_menu` VALUES (76, '评论列表修改', 73, NULL, NULL, NULL, 0, 2, 1, 1, 'system:comment:update', NULL, '', '2022-05-10 16:50:06', '2022-05-10 16:50:06', NULL);

-- ----------------------------
-- Table structure for t_reply
-- ----------------------------
DROP TABLE IF EXISTS `t_reply`;
CREATE TABLE `t_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comm_id` int(11) NOT NULL COMMENT '回复评论id',
  `reply_target_id` int(11) NOT NULL COMMENT '回复目标id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回复 内容',
  `from_uid` int(11) NOT NULL COMMENT '回复用户id',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_reply
-- ----------------------------
INSERT INTO `t_reply` VALUES (1, 1, 27, '回复27测试', 57, '2022-05-09 14:57:20', '2022-05-09 14:57:22', NULL);

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `level` int(11) NOT NULL DEFAULT 99 COMMENT '角色等级 越小级别越高',
  `status` tinyint(1) NOT NULL COMMENT '角色状态 0 停用  1 正常',
  `identification` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '对应的英文单词',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, '超级管理员', 1, 1, 'SuperAdmin', '管理网站,拥有网站的所有权限', '2022-02-20 16:03:00', '2022-05-13 18:14:10', NULL);
INSERT INTO `t_role` VALUES (2, '管理员', 2, 1, 'Admin', '负责管理数据的增删改查', '2022-04-30 16:03:00', '2022-05-13 18:14:51', NULL);
INSERT INTO `t_role` VALUES (3, '普通用户', 3, 1, 'User', '浏览网站', '2022-05-01 11:16:49', '2022-05-11 23:16:18', NULL);
INSERT INTO `t_role` VALUES (7, '测试', 4, 1, 'test', '测试', '2022-05-01 16:50:14', '2022-05-13 18:16:52', NULL);
INSERT INTO `t_role` VALUES (8, '测试2', 5, 1, 'test2', NULL, '2022-05-08 15:46:45', '2022-05-08 15:46:45', '2022-05-08 16:06:12');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (1, 2);
INSERT INTO `t_role_menu` VALUES (1, 5);
INSERT INTO `t_role_menu` VALUES (1, 6);
INSERT INTO `t_role_menu` VALUES (1, 21);
INSERT INTO `t_role_menu` VALUES (1, 22);
INSERT INTO `t_role_menu` VALUES (1, 23);
INSERT INTO `t_role_menu` VALUES (1, 24);
INSERT INTO `t_role_menu` VALUES (1, 25);
INSERT INTO `t_role_menu` VALUES (1, 27);
INSERT INTO `t_role_menu` VALUES (1, 28);
INSERT INTO `t_role_menu` VALUES (1, 29);
INSERT INTO `t_role_menu` VALUES (1, 30);
INSERT INTO `t_role_menu` VALUES (1, 31);
INSERT INTO `t_role_menu` VALUES (1, 32);
INSERT INTO `t_role_menu` VALUES (1, 33);
INSERT INTO `t_role_menu` VALUES (1, 34);
INSERT INTO `t_role_menu` VALUES (1, 35);
INSERT INTO `t_role_menu` VALUES (1, 36);
INSERT INTO `t_role_menu` VALUES (1, 37);
INSERT INTO `t_role_menu` VALUES (1, 38);
INSERT INTO `t_role_menu` VALUES (1, 70);
INSERT INTO `t_role_menu` VALUES (1, 3);
INSERT INTO `t_role_menu` VALUES (1, 41);
INSERT INTO `t_role_menu` VALUES (1, 44);
INSERT INTO `t_role_menu` VALUES (1, 45);
INSERT INTO `t_role_menu` VALUES (1, 46);
INSERT INTO `t_role_menu` VALUES (1, 47);
INSERT INTO `t_role_menu` VALUES (1, 48);
INSERT INTO `t_role_menu` VALUES (1, 49);
INSERT INTO `t_role_menu` VALUES (1, 50);
INSERT INTO `t_role_menu` VALUES (1, 52);
INSERT INTO `t_role_menu` VALUES (1, 53);
INSERT INTO `t_role_menu` VALUES (1, 54);
INSERT INTO `t_role_menu` VALUES (1, 55);
INSERT INTO `t_role_menu` VALUES (1, 56);
INSERT INTO `t_role_menu` VALUES (1, 57);
INSERT INTO `t_role_menu` VALUES (1, 72);
INSERT INTO `t_role_menu` VALUES (1, 73);
INSERT INTO `t_role_menu` VALUES (1, 74);
INSERT INTO `t_role_menu` VALUES (1, 75);
INSERT INTO `t_role_menu` VALUES (1, 76);
INSERT INTO `t_role_menu` VALUES (2, 3);
INSERT INTO `t_role_menu` VALUES (2, 41);
INSERT INTO `t_role_menu` VALUES (2, 44);
INSERT INTO `t_role_menu` VALUES (2, 45);
INSERT INTO `t_role_menu` VALUES (2, 46);
INSERT INTO `t_role_menu` VALUES (2, 47);
INSERT INTO `t_role_menu` VALUES (2, 48);
INSERT INTO `t_role_menu` VALUES (2, 49);
INSERT INTO `t_role_menu` VALUES (2, 50);
INSERT INTO `t_role_menu` VALUES (2, 52);
INSERT INTO `t_role_menu` VALUES (2, 53);
INSERT INTO `t_role_menu` VALUES (2, 54);
INSERT INTO `t_role_menu` VALUES (2, 55);
INSERT INTO `t_role_menu` VALUES (2, 56);
INSERT INTO `t_role_menu` VALUES (2, 57);
INSERT INTO `t_role_menu` VALUES (2, 72);
INSERT INTO `t_role_menu` VALUES (2, 73);
INSERT INTO `t_role_menu` VALUES (2, 74);
INSERT INTO `t_role_menu` VALUES (2, 75);
INSERT INTO `t_role_menu` VALUES (2, 76);
INSERT INTO `t_role_menu` VALUES (7, 3);
INSERT INTO `t_role_menu` VALUES (7, 41);
INSERT INTO `t_role_menu` VALUES (7, 50);
INSERT INTO `t_role_menu` VALUES (7, 72);
INSERT INTO `t_role_menu` VALUES (7, 73);
INSERT INTO `t_role_menu` VALUES (7, 47);
INSERT INTO `t_role_menu` VALUES (7, 55);
INSERT INTO `t_role_menu` VALUES (7, 74);

-- ----------------------------
-- Table structure for t_role_user
-- ----------------------------
DROP TABLE IF EXISTS `t_role_user`;
CREATE TABLE `t_role_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 309 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_user
-- ----------------------------
INSERT INTO `t_role_user` VALUES (166, 2, 57);
INSERT INTO `t_role_user` VALUES (174, 3, 28);
INSERT INTO `t_role_user` VALUES (254, 3, 28);
INSERT INTO `t_role_user` VALUES (263, 7, 56);
INSERT INTO `t_role_user` VALUES (265, 3, 29);
INSERT INTO `t_role_user` VALUES (276, 7, 30);
INSERT INTO `t_role_user` VALUES (277, 3, 30);
INSERT INTO `t_role_user` VALUES (278, 3, 31);
INSERT INTO `t_role_user` VALUES (279, 7, 31);
INSERT INTO `t_role_user` VALUES (292, 2, 69);
INSERT INTO `t_role_user` VALUES (297, 7, 74);
INSERT INTO `t_role_user` VALUES (298, 3, 75);
INSERT INTO `t_role_user` VALUES (301, 2, 33);
INSERT INTO `t_role_user` VALUES (302, 7, 55);
INSERT INTO `t_role_user` VALUES (303, 3, 70);
INSERT INTO `t_role_user` VALUES (304, 3, 71);
INSERT INTO `t_role_user` VALUES (305, 3, 73);
INSERT INTO `t_role_user` VALUES (306, 3, 72);
INSERT INTO `t_role_user` VALUES (307, 2, 27);
INSERT INTO `t_role_user` VALUES (308, 1, 32);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称(昵称)',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '用户状态',
  `phone` bigint(11) NULL DEFAULT NULL COMMENT '联系方式',
  `gender` int(1) NULL DEFAULT 0 COMMENT '性别  { 0: \'保密\', 1: \'男\', 2: \'女\' }',
  `createdAt` datetime NULL DEFAULT NULL,
  `updatedAt` datetime NULL DEFAULT NULL,
  `deletedAt` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (27, '工具人', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984031029263.jpg', 1, 17709674231, 0, '2022-04-20 20:21:39', '2022-05-11 21:44:08', NULL);
INSERT INTO `t_user` VALUES (28, '211', '$2a$10$ZossTaKC8Um/Zo.o7J7WU.XBdM3AzxrqCbFKuVch9jwfXKxgOxJOe', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984391387965.jpg', 1, 17709674232, 0, '2022-04-20 20:21:42', '2022-05-08 12:33:11', NULL);
INSERT INTO `t_user` VALUES (29, '贺刚', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984398143284.jpg', 1, 17709674233, 0, '2022-04-20 20:21:49', '2022-05-08 12:33:18', NULL);
INSERT INTO `t_user` VALUES (30, '2GD11', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984414794535.jpg', 1, 17709674234, 0, '2022-04-20 20:21:56', '2022-05-08 13:38:39', NULL);
INSERT INTO `t_user` VALUES (31, '2G1', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'https://tse1-mm.cn.bing.net/th/id/OIP-C.kgO9vKRBreceGgFw-YoZaAAAAA?pid=ImgDet&rs=1', 1, 17709674235, 1, '2022-04-20 20:22:00', '2022-05-08 13:41:35', NULL);
INSERT INTO `t_user` VALUES (32, 'admin', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\12\\1652363661300282.jpg', 1, 17709674236, 0, '2022-04-21 16:37:23', '2022-05-13 16:51:49', NULL);
INSERT INTO `t_user` VALUES (33, 'llvyr', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\12\\1652339812133889.jpg', 1, 17709674237, 0, '2022-04-21 16:38:53', '2022-05-12 15:16:52', NULL);
INSERT INTO `t_user` VALUES (55, 'test', '$2a$10$hWvbOZ1JnoquGPFSFVSX8ubNoVJf4g.OMOoqiRWGHndkNwAZrtcfa', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984379704707.jpg', 1, 17709674238, 1, '2022-04-29 11:39:55', '2022-05-11 23:09:01', NULL);
INSERT INTO `t_user` VALUES (56, '韩秀英', '$2a$10$LarJarwXWD/BGl.pEl.e1uMvo43.UuWnqY/Q1XgiqxhBE/j/toCHu', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\08\\1651984407839522.jpg', 1, 17709674239, 2, '2022-04-30 16:03:29', '2022-05-08 12:33:27', NULL);
INSERT INTO `t_user` VALUES (57, '1234', '$2a$10$xXS4X1M6e3NQ0rWE4xhxiO7l9.X.wLue.Sm4RZIfRb1Yxa4iIG.di', 'http://127.0.0.1:7001/public\\uploads\\avatar\\2022\\05\\11\\1652256674111479.jpg', 1, 17709674212, 1, '2022-04-30 23:26:39', '2022-05-11 16:11:14', NULL);
INSERT INTO `t_user` VALUES (62, '袁强', '$2a$10$37fXeVkMYxK1WzamYZMBhO.J3iFkTlcDOTRv/KrU6DLB70T0EdwrO', 'http://dummyimage.com/100x100', 1, 17709674213, 1, '2022-05-07 09:27:30', '2022-05-08 15:20:17', NULL);
INSERT INTO `t_user` VALUES (69, '赖磊1', '$2a$10$NQM5mh.1SmKg3AnvnS68EOjIMbVlBnG9Q8DaoiYFPkdolhCkeq0Ey', 'http://dummyimage.com/100x100', 1, 17709674214, 0, '2022-05-08 16:24:36', '2022-05-08 16:24:36', NULL);
INSERT INTO `t_user` VALUES (70, '远行乀', '$2a$10$4bvuMcEF1rWK50yPlg7HG.6RCMHK.IDJbimVUNX2kDxY.LLJ.zv8W', 'http://dummyimage.com/100x100', 1, 17709674215, 1, '2022-05-10 08:58:23', '2022-05-11 23:09:13', NULL);
INSERT INTO `t_user` VALUES (71, '远行', '$2a$10$PDY3kvmo9.wyZFIkbokg0.tXs34of/Em09.lkv8OIZmuczFlH21De', 'http://dummyimage.com/100x100', 1, 17709674216, 1, '2022-05-10 09:00:15', '2022-05-11 23:09:18', NULL);
INSERT INTO `t_user` VALUES (72, '邓娜', '$2a$10$rWA5jXgXGQv7ctIflE6hee942IfUUHG4BOtGclIyaMVDHZCiKIhu6', 'http://dummyimage.com/100x100', 1, 17709674217, 0, '2022-05-10 09:01:24', '2022-05-11 23:09:26', NULL);
INSERT INTO `t_user` VALUES (73, '邓娜1', '$2a$10$xGhXXwGAO6QOc8KKul13p./YCv8FoolKpEzSaW/oRRl3DVZ62qkvy', 'http://dummyimage.com/100x100', 1, 17709674218, 0, '2022-05-10 09:02:44', '2022-05-11 23:09:22', NULL);
INSERT INTO `t_user` VALUES (74, '测试11', '$2a$10$kIB5uy2BrYQ6NT5B5AplUOslUClms1XRJzkJH8evcbp9II9zlwRwi', NULL, 1, 17709674219, 0, '2022-05-10 13:09:46', '2022-05-10 13:09:46', NULL);
INSERT INTO `t_user` VALUES (75, '贺刚1', '$2a$10$pSQGCTaCHsDp6paY/9fo1uf1/PuGav9nRYzR/oDsTxPrdBkayz.o.', 'https://tse1-mm.cn.bing.net/th/id/OIP-C.kgO9vKRBreceGgFw-YoZaAAAAA?pid=ImgDet&rs=1', 1, 2147483647, 0, '2022-04-20 08:00:00', '2022-05-11 16:28:58', NULL);

SET FOREIGN_KEY_CHECKS = 1;
