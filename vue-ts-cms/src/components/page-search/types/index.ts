//  @/base-ui/form/types 中的定义的类型
// type IFormItemType = 'input' | 'password' | 'select' | 'datepicker'
// interface IFormItem {
//   field: string
//   label?: string
//   rule?: any[]
//   type?: IFormItemType
//   placeholder?: any[]
//   options?: any[]
//   otherOptions?: any
// }
// interface IFormConfig {
//   labelWidth?: string
//   labelPosition?: string
//   itemStyle?: object
//   colLayout?: object
//   formItems: IFormItem[]
// }

// export { IFormConfig }



export * from "@/base-ui/form/types"
