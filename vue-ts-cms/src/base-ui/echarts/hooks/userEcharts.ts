import * as echarts from "echarts"
import type { EChartsOption } from 'echarts'
import chinaMap from "../data/china-map.json"
echarts.registerMap('china', chinaMap)
export default function (echartsRef: HTMLElement) {
  const echartsInstance = echarts.init(echartsRef)

  const setOptions = (options: EChartsOption) => {

    echartsInstance.setOption(options);
  }

  window.addEventListener('resize', () => {
    echartsInstance.resize()
  })

  return {
    echartsInstance,
    setOptions
  }

}
