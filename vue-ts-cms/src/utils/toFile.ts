//将base64转换为blob
export function dataURLtoBlob(dataUrl: any) {
  var arr = dataUrl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}
//将blob转换为file
export function blobToFile(theBlob: any, fileName: any) {
  theBlob.lastModifiedDate = new Date();
  theBlob.name = fileName;
  return theBlob;
}

export function base64ToFile(base64Data: any, imgName: string) {
  //调用
  const blob = dataURLtoBlob(base64Data);
  const file = blobToFile(blob, imgName);

  return file
}


