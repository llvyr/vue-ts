const debounce = (fun: any, delay: number, before: any) => {
  let timer: any = null
  return (params: any) => {
    timer && window.clearTimeout(timer)
    before && before(params)
    timer = window.setTimeout(() => {
      fun()
      timer = null
    }, delay)
  }
}
export {
  debounce
}
