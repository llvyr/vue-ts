import { App } from 'vue'
import SvgIcon from '@/components/svg-icon'

const req = require.context('./svg', false, /\.svg$/)
const requireAll = (requireContext: __WebpackModuleApi.RequireContext) => requireContext.keys().map(requireContext)
requireAll(req)

export function registerIcons(app: App) {
  app.component('svg-icon', SvgIcon)

}
