export interface DataType<T = any> {
  code: number
  message: string
  data: T

}
export interface IQueryInfo {
  queryInfo: {
    pageSize?: string | number,
    currentPage?: string | number,
    otherWhere?: Object
  }
}

export interface IDelAllInfo {
  ids: number[]
}

