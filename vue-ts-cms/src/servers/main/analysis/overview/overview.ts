import myRequest from "@/servers/index"
import { DataType } from "@/servers/types"
enum OverviewAPI {
  userByRole = "/analysis/userByRole",
  userGroupByGender = "/analysis/userGroupByGender"
}
export const getUserByRole = () => {
  return myRequest.get<DataType>({
    url: OverviewAPI.userByRole,

  })
}
export const getUserGroupByGender = () => {
  return myRequest.get<DataType>({
    url: OverviewAPI.userGroupByGender,

  })
}
