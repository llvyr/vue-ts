import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
import store from "@/store"

enum UserAPI {
  userList = "/users/userList",
  userDelAll = '/users/delAll',
  userCreate = '/users',
  userEdit = '/users/',
  createdBatch = '/users/bulkCreate',
  restPassword = '/users/resetPassword',
  updateAvatar = '/file/uploadAvatar/'
}
export const getUserList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: UserAPI.userList,
    data: queryInfo,
  })
}

export const delUserAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: UserAPI.userDelAll,
    data: { ids }
  })
}

export const createOrEditUser = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: UserAPI.userEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: UserAPI.userCreate,
      data: payload
    })
  }
}

export const createdBatchUser = (arr: any[]) => {
  return myRequest.post<DataType>({
    url: UserAPI.createdBatch,
    data: arr
  })
}
export const resetPassword = (payload: any) => {
  return myRequest.post<DataType>({
    url: UserAPI.restPassword,
    data: payload
  })
}


export const updateAvatar = (id: number, formData: FormData, store: any) => {
  return myRequest.post<DataType>({
    url: UserAPI.updateAvatar + id,
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + store.state.login.token['token']
    },
  })
}





