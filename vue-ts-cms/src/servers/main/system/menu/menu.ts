import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
enum MenuAPI {
  treeMenus = "/menus/treeMenus",
  menuCreate = '/menus',
  menuEdit = '/menus/',
  menuDelAll = '/menus/delAll',

}
export const getTreeMenuList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: MenuAPI.treeMenus,
    data: queryInfo,
  })
}

export const createOrEditMenu = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: MenuAPI.menuEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: MenuAPI.menuCreate,
      data: payload
    })
  }
}


export const delMenuAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: MenuAPI.menuDelAll,
    data: { ids }
  })
}
