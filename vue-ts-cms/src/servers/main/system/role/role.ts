import myRequest from "@/servers/index"
import { DataType, IQueryInfo } from "@/servers/types"
enum RoleAPI {
  roleList = "/roles/roleList",
  roleDelAll = '/roles/delAll',
  roleCreate = '/roles',
  roleEdit = '/roles/',
  createdBatch = '/roles/bulkCreate'
}
export const getRoleList = (queryInfo?: IQueryInfo) => {
  return myRequest.post<DataType>({
    url: RoleAPI.roleList,
    data: queryInfo,
  })
}


export const delRoleAll = (ids: number[]) => {
  return myRequest.delete<DataType>({
    url: RoleAPI.roleDelAll,
    data: { ids }
  })
}

export const createOrEditRole = (payload: any) => {
  if (payload.id) {
    delete payload.password
    return myRequest.put<DataType>({
      url: RoleAPI.roleEdit + payload.id,
      data: payload
    })
  } else {
    return myRequest.post<DataType>({
      url: RoleAPI.roleCreate,
      data: payload
    })
  }
}
export const createdBatchRole = (arr: any[]) => {
  return myRequest.post<DataType>({
    url: RoleAPI.createdBatch,
    data: arr
  })
}
