import type { AxiosRequestConfig, AxiosResponse } from 'axios'

interface MyRequestIntercepts<T = AxiosResponse> {
  requestHook?: (config: AxiosRequestConfig) => AxiosRequestConfig
  requestHookCatch?: (error: any) => any
  responseHook?: (config: T) => T
  responseHookCatch?: (error: any) => any
}

interface MyRequestConfig<T = AxiosResponse> extends AxiosRequestConfig {
  intercepts?: MyRequestIntercepts<T>
  showLoading?: boolean
}

export { MyRequestConfig, MyRequestIntercepts }
