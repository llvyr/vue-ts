import { IAccount, ILoginResType } from './types'
import { DataType } from '@/servers/types'
import myRequest from "../index"
enum LoginAPI {
  AccountLogin = "/loginAccount",
  LoginUserInfoById = "/userById/",
  LoginUserInfo = "/users/userInfo",
  LoginMenuByRoles = "/menus/treeMenusByRoleIds",
  createdCode = "/createdCode",
  PhoneLogin = "/loginPhone"
}
//获取手机验证码
export const getVerifyCode = () => {
  return myRequest.get({
    url: LoginAPI.createdCode,
    showLoading: false
  })
}
//账号登录
export const loginAccount = (account: IAccount) => {
  return myRequest.post<DataType<ILoginResType>>({
    url: LoginAPI.AccountLogin,
    data: account,
    showLoading: false
  })
}
//手机登录
export const loginPhone = (phone: any) => {
  return myRequest.post<DataType<ILoginResType>>({
    url: LoginAPI.PhoneLogin,
    data: phone,
    showLoading: false
  })
}
// 请求用户信息通过id
export const getUserInfoById = (id: number) => {
  return myRequest.get<DataType>({
    url: LoginAPI.LoginUserInfoById + id,
    showLoading: false
  })
}
// 请求用户信息通过解析token
export const getUserInfo = () => {
  return myRequest.get<DataType>({
    url: LoginAPI.LoginUserInfo,
    showLoading: false
  })
}
// 根据用户角色获取菜单
export const getMenuByRoles = (ids: Array<number>) => {
  return myRequest.post<DataType>({
    url: LoginAPI.LoginMenuByRoles,
    data: { ids },
    showLoading: false
  })
}

