

export interface IAccount {
  account: string
  password: string
}
export interface ILoginResType {
  id: number
  username: string
  password: string
  avatar: string
  phone: string
  token: string
}
