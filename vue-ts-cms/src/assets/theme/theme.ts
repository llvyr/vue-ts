import { themes } from './model'
// 修改页面中的样式变量值
const changeStyle = (obj: any) => {
  for (const key in obj) {
    document
      .getElementsByTagName('body')[0]
      .style.setProperty(`--${key}`, obj[key])
  }
}
// 改变主题的方法
export const setTheme = (themeName: string) => {


  // localStorage.setItem('theme', themeName) // 保存主题到本地，下次进入使用该主题
  const themeConfig = themes[themeName]
  // 如果有主题名称，那么则采用我们定义的主题
  if (themeConfig) {
    for (const key in themeConfig) {
      if (Object.prototype.hasOwnProperty.call(themeConfig, key)) {
        const element = themeConfig[key];
        localStorage.setItem(key, element)
      }
    }
    changeStyle(themeConfig) // 改变样式
  } else {
    /**
     * 左侧菜单背景颜色
     * 左侧菜单标题颜色
     * 头部字体颜色
     * 头部背景颜色
     * 子菜单默认字体颜色
     * 菜单激活颜色
     */
    const arr: string[] = ['primaryMenuColor', 'primaryMenuTextColor', 'primaryHeaderColor', 'primaryHeaderBgColor', 'color', 'activeColor']
    const themeConfig = {}
    arr.forEach(item => {
      themeConfig[item] = localStorage.getItem(item)
    })
    changeStyle(themeConfig)
  }
}
