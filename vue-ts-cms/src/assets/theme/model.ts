// 一套默认主题以及一套暗黑主题
// 一套默认主题以及一套暗黑主题
export const themes = {
  default: {
    primaryMenuColor: `${0}, ${21},${43}`,
    primaryMenuTextColor: `${255}, ${255},${255}`,
    primaryHeaderBgColor: `${0}, ${21},${43}`,
    primaryHeaderColor: `${255}, ${255},${255}`,
    primaryMenuHoverColor: `${255}, ${255},${255}`,
    color: `${255}, ${255},${255}`,
    activeColor: `${3}, ${94},${193}`

  },
  green: {
    primaryMenuColor: `${15}, ${41},${80}`,
    primaryMenuTextColor: `${163},${177},${204}`,
    primaryHeaderColor: `${255}, ${255},${255}`,
    primaryHeaderBgColor: `${0}, ${166},${90}`,
    primaryMenuHoverColor: `${255}, ${255},${255}`,
    color: `${255}, ${255},${255}`,
    activeColor: `${112}, ${111},${211}`
  }
}

