import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import store, { setupStore } from './store'
// 基础样式导入
import 'normalize.css'
import './assets/less/index.less'
// element-plus 基础样式导入
import 'element-plus/lib/theme-chalk/el-message.css'
import "element-plus/lib/theme-chalk/index.css"




import ElComponents from "@/plugins"

// 兼容 mousewheel' event'
import 'default-passive-events'
// 导入全局注册属性
import { globalRegister } from "@/global"
// 注册字体图标库
import * as ElIconModules from '@element-plus/icons-vue'
const app = createApp(App)
// 统一注册Icon图标
for (const iconName in ElIconModules) {
  if (Reflect.has(ElIconModules, iconName)) {
    const item = ElIconModules[iconName]
    app.component(iconName, item)
  }
}

setupStore() // vuex 刷新不丢失
app.use(router)
app.use(store)

app.use(globalRegister)


ElComponents(app)
app.mount('#app')
