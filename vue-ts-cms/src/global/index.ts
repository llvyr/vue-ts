import { App } from 'vue'
import { registerProperties } from "./global.properties"
import { registerComponents } from "./global.cpns"
import { registerIcons } from "../icons"
export function globalRegister(app: App) {
  registerProperties(app)
  registerComponents(app)
  registerIcons(app)
}
