import { App } from 'vue'
import { formDateUtcString, formDateTimestampString } from "@/utils/FormDate"
export function registerProperties(app: App) {
  app.config.globalProperties.$filters = {
    formDateUTC(UtCSting: any, format: string) {
      return formDateUtcString(UtCSting, format)
    },
    formDateTimestamp(Timestamp: any, format: string) {
      return formDateTimestampString(Timestamp, format)
    }
  }

}

