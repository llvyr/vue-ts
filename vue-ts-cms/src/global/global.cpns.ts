import { App } from 'vue'
import BaseTable from "@/base-ui/table"
import BaseForm from "@/base-ui/form"
import PageSearch from "@/components/page-search"
import PageTable from "@/components/page-table"
import PageModal from "@/components/page-modal"
import PageDrawer from "@/components/page-drawer"
import ExportData from "@/components/Excel/exportData.vue"
import ImportData from "@/components/Excel/importData.vue"
import SvgIcon from "@/components/svg-icon"


export function registerComponents(app: App) {
  app.component('BaseForm', BaseForm)
  app.component('BaseTable', BaseTable)
  app.component('PageSearch', PageSearch)
  app.component('PageTable', PageTable)
  app.component('PageModal', PageModal)
  app.component('PageDrawer', PageDrawer)
  app.component('ExportData', ExportData)
  app.component('ImportData', ImportData)
  app.component('SvgIcon', SvgIcon)
}

