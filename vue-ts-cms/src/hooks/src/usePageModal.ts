import PageModal from "@/components/page-modal"
import { ref } from "vue"
type callbackType = (item?: any) => void
export function usePageModal(callbackEdit?: callbackType, callbackCreated?: callbackType): any {
  const pageModalRef = ref<InstanceType<typeof PageModal>>()
  let defaultInfo = ref({})
  // 编辑或者新建
  const handleCreateOrEdit = (val: any) => {
    if (pageModalRef.value) {
      // 将值置空 防止关闭后因为有引用的数据 弹框实例不能被销毁
      if (val) {
        defaultInfo.value = { ...val }
        callbackEdit && callbackEdit(val)
      } else {
        defaultInfo.value = {}
        callbackCreated && callbackCreated()
      }
      pageModalRef.value.isShowDialog = true
    }
  }

  return [handleCreateOrEdit, defaultInfo, pageModalRef]
}
