import { computed } from 'vue'
import { mapState, useStore } from 'vuex'


/**
 * @param {Array || Object} mapper
 * @returns {Array} storeState
 */
export function useState(mapper: any) {
  // vue3的setup中没有绑定this（无法通过this.$store获取到）,通过useStore获取store对象
  const store = useStore()

  // mapState返回的是一系列的function:{name:nameFn,age:ageFn}的对象
  const storeStateFns = mapState(mapper)

  // 对数据进行转换

  // 存放最终可以在模板上使用的数据 {{ name }}
  const storeState: any = []
  // 在组件中使用vuex store中的数据，应该将mapState返回的数据存放到computed中
  Object.keys(storeStateFns).forEach(fnKey => {
    // mapState中获取到的每一个函数内部其实都是通过this.$store.state.name····进行访问的，但是在setup中并没有this.$store,所以通过bind将store对象绑定到每一个mapState中的函数中
    const fn = storeStateFns[fnKey].bind({ $store: store })
    // 将获取到的所有state存放到storeState中
    storeState[fnKey] = computed(fn)
  })

  return storeState
}


/**
 *
// const storeState2 = useState({
//   sName: state => state.login.menus

// })
// console.log(storeState2.sName.value);

 *
 */
