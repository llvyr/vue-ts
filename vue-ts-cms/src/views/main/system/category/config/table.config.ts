import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTableConfig, baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "文章分类",
  ...baseTableConfig,
  propsList: [
    {
      prop: "name",
      minWidth: "60",
      label: "分类名称",
      slotName: "name",


    },
    {
      prop: "identification",
      minWidth: "60",
      label: "标识",
      slotName: "identification"
    },
    {
      prop: "status",
      minWidth: "100",
      label: "状态",
      slotName: "status"
    },

    {
      prop: "remark",
      minWidth: "100",
      label: "分类描述",
      slotName: "remark",
      showOverflowTooltip: true
    },

    {
      prop: "createdAt",
      minWidth: "120",
      label: "创建时间",
      slotName: "createdAt",
      showOverflowTooltip: true
    },
    {
      label: "操作",
      minWidth: "180",
      slotName: "handle"
    }


  ]
}
export { tableConfig }
