import { IFormConfig } from "@/base-ui/form";

const modalFormConfig: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {},
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'name',
      label: "角色名",
      type: "input",
      placeholder: "请输角色名"
    },
    {
      field: 'identification',
      label: "角色标识符",
      type: "input",
      placeholder: "角色标识符"
    },
    {
      field: 'description',
      label: "角色描述",
      type: "textarea",
      placeholder: "请输角色描述"
    },

    {
      field: 'status',
      label: "角色状态",
      type: "radio",
      defaultValue: true,
      options: [
        {
          value: true,
          title: "启用",
        },
        {
          value: false,
          title: "禁用",
        },

      ]
    },



  ]
}
export { modalFormConfig }
