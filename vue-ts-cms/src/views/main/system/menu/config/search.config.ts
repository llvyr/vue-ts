import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'menu_name',
      label: "菜单名称",
      type: "input",
      placeholder: "请输入菜单名称"
    },
    {
      field: 'remark',
      label: "菜单标识",
      type: "input",
      placeholder: "请输入标识"
    },
    {
      field: 'path',
      label: "菜单路径",
      type: "input",
      placeholder: "请输入菜单路径"
    },
    {
      field: 'menu_type',
      label: "菜单类型",
      type: "select",
      options: [
        {
          value: 0,
          title: "一级菜单",
        },
        {
          value: 1,
          title: "二级菜单",
        },
        {
          value: 2,
          title: "按钮",
        }
      ]

    },
    {
      field: 'status',
      label: "菜单状态",
      type: "select",
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        }
      ]
    },
    ...baseSearchFormItems
  ]
}
export { searchFormConfig }
