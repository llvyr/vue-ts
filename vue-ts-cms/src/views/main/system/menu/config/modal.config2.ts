import { IFormConfig } from "@/base-ui/form";

const modalFormConfig2: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {
    marginTop: '-10px'
  },
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'menu_name',
      label: "菜单名称",
      type: "input",
      isHidden: false,
      placeholder: "请输菜单名称"
    },
    {
      field: 'component',
      label: "组件名称",
      type: "input",
      isHidden: false,
      placeholder: "请输组件名称"
    },
    {
      field: 'path',
      label: "路径",
      type: "input",
      isHidden: false,
      placeholder: "请输组件路径"
    },
    {
      field: 'menu_type',
      label: "菜单类别",
      type: "radio",
      isHidden: false,
      otherOptions: {
        disabled: true
      },
      defaultValue: 0,
      options: [
        {
          value: 0,
          title: "一级菜单",
        },
        {
          value: 1,
          title: "二级菜单",
        },
        {
          value: 2,
          title: "按钮",
        }
      ]
    },
    {
      field: 'visible',
      label: "是否显示",
      type: "radio",
      isHidden: false,
      defaultValue: true,
      options: [
        {
          value: true,
          title: "显示",
        },
        {
          value: false,
          title: "隐藏",
        },

      ]
    },
    {
      field: 'is_cache',
      label: "是否缓存",
      type: "radio",
      defaultValue: true,
      isHidden: false,
      options: [
        {
          value: true,
          title: "缓存",
        },
        {
          value: false,
          title: "不缓存",
        },
      ]
    },
    {
      field: 'status',
      label: "是否启用",
      type: "radio",
      defaultValue: 1,
      isHidden: false,
      options: [
        {
          value: 1,
          title: "启用",
        },
        {
          value: 0,
          title: "禁用",
        },
      ]
    },
    {
      field: 'perms',
      label: "权限标识",
      type: "input",
      isHidden: false,
      placeholder: "格式为:system:xx:xxx"
    },
    {
      field: 'icon',
      label: "图标",
      type: "autocomplete",
      isHidden: false,
      options: []
    },
    {
      field: 'remark',
      label: "描述",
      isHidden: false,
      type: "textarea",
      placeholder: "请输入权限描述"
    },
  ]
}
export { modalFormConfig2 }
