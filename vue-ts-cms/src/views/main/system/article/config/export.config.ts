export const ARTICLE_IMPORT = {
  文章标题: 'title',
  文章封面: 'cover',
  文章简介: 'summary',
  文章级别: 'level',
  文章分类: 'associateIds',
  文章浏览量: 'view_count',
  文章内容: 'content',
  文章创建时间: 'createdAt',

}


export const ARTICLE_EXPORT = {
  文章标题: 'title',
  文章封面: 'cover',
  文章简介: 'summary',
  文章级别: 'level',
  文章浏览量: 'view_count',
  文章内容: 'content',
  文章分类: 'categories',
  文章创建时间: 'createdAt',

}
