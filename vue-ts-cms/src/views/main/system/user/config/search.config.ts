import { IFormConfig } from "@/base-ui/form";
import { baseSearchFormConfig, baseSearchFormItems } from "../../base-config"
const searchFormConfig: IFormConfig = {
  ...baseSearchFormConfig,
  formItems: [
    {
      field: 'username',
      label: "用户名",
      type: "input",
      placeholder: "请输用户名"
    },
    {
      field: 'phone',
      label: "手机号",
      type: "input",
      placeholder: "请输入手机号"
    },
    {
      field: 'status',
      label: "用户状态",
      type: "select",
      options: [
        {
          value: true,
          title: "启用",
        },
        {
          value: false,
          title: "禁用",
        }
      ]
    },
    {
      field: 'associateModal',
      isAssociate: true,
      associateKey: 'identification',
      label: "用户角色",
      type: "select",
      options: []
    },
    {
      field: 'gender',
      label: "性别",
      type: "select",
      options: [
        {
          value: 0,
          title: "保密",
        },
        {
          value: 1,
          title: "男",
        },
        {
          value: 2,
          title: "女",
        }
      ]
    },
    ...baseSearchFormItems

  ]
}
export { searchFormConfig }
