import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTableConfig, baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "用户表",

  ...baseTableConfig,

  propsList: [
    {
      prop: "username",
      minWidth: "60",
      label: "用户名",
      slotName: "username",


    },
    {
      prop: "avatar",
      minWidth: "100",
      label: "头像",
      slotName: "avatar"
    },
    {
      prop: "gender_text",
      minWidth: "60",
      label: "性别",
      slotName: "gender_text"
    },
    {
      prop: "status",
      minWidth: "100",
      label: "状态",
      slotName: "status"
    },
    {
      prop: "role",
      minWidth: "100",
      label: "角色",
      slotName: "role",
      align: 'left'
    },
    {
      prop: "phone",
      minWidth: "100",
      label: "手机号",
      slotName: "phone",
      showOverflowTooltip: true
    },

    {
      prop: "createdAt",
      minWidth: "120",
      label: "创建时间",
      slotName: "createdAt",
      showOverflowTooltip: true
    },
    {
      label: "操作",
      minWidth: "240",
      slotName: "handle"
    }


  ]
}
export { tableConfig }
