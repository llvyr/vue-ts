import { IFormConfig } from "@/base-ui/form";

const modalFormConfig: IFormConfig = {
  labelWidth: '100px',
  labelPosition: 'left',
  itemStyle: {},
  colLayout: {
    span: 24
  },
  formItems: [
    {
      field: 'username',
      label: "用户名",
      type: "input",
      placeholder: "请输用户名"
    },
    {
      field: 'password',
      label: "密码",
      type: "password",
      isHidden: true,
      placeholder: "请输密码"
    },
    {
      field: 'phone',
      label: "手机号",
      type: "input",
      placeholder: "请输入手机号"
    },
    {
      field: 'associateIds',
      label: "用户角色",
      type: "checkbox",
      defaultValue: [2],
      isAssociate: true,
      associateName: 'userRoles',
      associateKey: 'id',
      options: [],
      otherOptions: {
        min: 1
      }
    },
    {
      field: 'gender',
      label: "性别",
      type: "radio",
      defaultValue: 0,
      options: [
        {
          value: 0,
          title: "保密",
        },
        {
          value: 1,
          title: "男",
        },
        {
          value: 2,
          title: "女",
        }
      ]
    },



  ]
}
export { modalFormConfig }
