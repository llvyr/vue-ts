import { ITableConfig } from "@/base-ui/table/types.ts"
import { baseTableConfig, baseTablePropsList } from "../../base-config"
const tableConfig: ITableConfig = {
  title: "文章评论",
  ...baseTableConfig,

  propsList: [
    {
      prop: "userDetail",
      minWidth: "60",
      label: "评论用户",
      slotName: "userDetail",
    },
    {
      prop: "articleDetail",
      minWidth: "100",
      label: "评论的文章",
      slotName: "articleDetail",
    },

    {
      prop: "content",
      minWidth: "160",
      label: "评论内容",
      slotName: "content"
    },
    {
      prop: "replies",
      minWidth: "100",
      label: "相关回复",
      slotName: "replies"
    },

    {
      prop: "createdAt",
      minWidth: "120",
      label: "创建时间",
      slotName: "createdAt",
      showOverflowTooltip: true
    },
    {
      label: "操作",
      minWidth: "180",
      slotName: "handle"
    }


  ]
}
export { tableConfig }
