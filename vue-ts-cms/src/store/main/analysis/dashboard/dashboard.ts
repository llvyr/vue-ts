import { Module } from "vuex"
import { IRootState, IDashboardSate } from "@/store/types"
import { getDashboardList } from "@/servers/main/analysis/dashboard/dashboard"
const DashboardStore: Module<IDashboardSate, IRootState> = {
  namespaced: true,
  state() {
    return {
      total: []
    }
  },
  mutations: {
    changeTotals(state, total) {
      state.total = total
    },
    reset(state) {
      state.total = []
    }
  },
  actions: {
    async TotalAction({ commit }) {
      const { code, data } = await getDashboardList()
      if (code == 1) {
        commit("changeTotals", data)
        return true
      }
      return false
    },
  }
}
export default DashboardStore
