interface IRoleItemState {
  count: number | string
  rows: any[]

}
export interface IRoleState {
  roleList: IRoleItemState
}
