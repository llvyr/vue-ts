import { Module } from "vuex";
import type { IRootState } from "@/store/types"
import type { IMenuState } from "@/store/main/system/menu/types"
import type { DataType, IQueryInfo, IDelAllInfo } from "@/servers/types"
import { getTreeMenuList, createOrEditMenu, delMenuAll } from "@/servers/main/system/menu/menu"
import { ElMessage } from "element-plus";
const MenuStore: Module<IMenuState, IRootState> = {
  namespaced: true,
  state() {
    return {
      menuList: { rows: [], count: 0 }
    }
  },
  mutations: {
    changeMenuList(state, menuList) {
      state.menuList = menuList
    },
    reset(state) {
      state.menuList = { rows: [], count: 0 }
    }
  },
  actions: {
    async menuListAction({ commit }, queryInfo?: IQueryInfo) {
      const { code, data } = await getTreeMenuList(queryInfo)
      if (code === 1) {
        commit('changeMenuList', data)
      }
    },
    async menuDelAllAction({ commit, dispatch }, { ids }: IDelAllInfo) {
      const { code, message } = await delMenuAll(ids)
      if (code == 1) {
        ElMessage.success(message)
        await dispatch('menuListAction', {
          queryInfo: {}
        })
      }
    },
    async menuCreateOrEditAction({ commit, dispatch }, { newVal, pageDrawerRef }) {
      const { code, message } = await createOrEditMenu(newVal)
      if (code == 1) {
        pageDrawerRef.value.isShowDrawer = false
        ElMessage.success(message)
        await dispatch('menuListAction', {
          queryInfo: {}
        })
        await dispatch("getEntireMenus", null, { root: true })
      }
    },

  },



}


export default MenuStore
