import { Module } from "vuex"
import type { IRootState, IDefaultConfigState } from "../types"
import { LStorage } from "@/utils/storage"
import { IS_FOLD } from "@/store/const"

const defaultConfigStore: Module<IDefaultConfigState, IRootState> = {
  namespaced: true,
  state() {
    return {
      isFold: false,
      theme: 'default'
    }
  },
  mutations: {
    changeIsFold(state, isFold: boolean) {
      state.isFold = isFold
      LStorage.set(IS_FOLD, isFold)
    },
    changeTheme(state, theme: string) {
      state.theme = theme
      LStorage.set('theme', theme)
    },
    reset(state) {
      state.theme = ''
    }
  },
  actions: {
    isFoldAction({ commit }, payload: boolean) {
      commit('changeIsFold', payload)

    },
    themeAction({ commit }, payload: string) {
      commit('changeTheme', payload)
    },
    // 保证vuex 刷新是有数据的
    loadStore({ commit }) {
      const isFold = LStorage.get(IS_FOLD)
      // console.log(isFold);
      if (isFold !== undefined && isFold != null) {
        commit('changeIsFold', isFold)
      }
      const theme = LStorage.get('theme')

      if (theme) {
        commit('changeTheme', theme)
      }
    }
  }

}

export default defaultConfigStore
