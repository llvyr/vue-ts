import { IUserState } from "@/store/main/system/user/types"
import { ILoginState } from "@/store/login/types"
import { IMenuState } from "./main/system/menu/types"
import { ICategoryState } from "./main/system/category/types"
import { IArticleState } from "./main/system/article/types"
import { ICommentState } from "./main/system/comment/types"
// 根路径接口
interface IRootState {

  queryInfo: any
  entireRoles: any[]
  entireMenus: any[]
  entireUsers: any[]
  entireCategorys: any[]
  entireArticles: any[]
}

interface IRootWidthModule {
  login: ILoginState
  user: IUserState
  menu: IMenuState
  dashboard: IDashboardSate
  overview: IOverViewSate
  defaultConfig: IDefaultConfigState
  article: IArticleState
  category: ICategoryState
  comment: ICommentState
}
// 默认配置类型接口
interface IDefaultConfigState {
  isFold: boolean,
  theme: string
}

// system类型接口
// 根类型接口扩展
export type IStoreType = IRootWidthModule & IRootState


interface IDashboardSate {
  total: any[]
}
interface IOverViewSate {
  userByRole: any[]
  userGroupByGender: any[]

}
export {
  IRootState,
  ILoginState,
  IDefaultConfigState,
  IDashboardSate,
  IOverViewSate,
  ICategoryState,
  IArticleState,
  ICommentState

}

